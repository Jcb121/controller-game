rsync --exclude 'node_modules' -avz -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --progress . root@47.91.92.20:~/controller-game
ssh root@47.91.92.20 <<'ENDSSH'
cd controller-game/
npm install
systemctl restart controller-game
ENDSSH