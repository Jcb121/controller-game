const path = require('path')

module.exports = {
  entry: {
    web: './targets/web/index.tsx',
    editor: './targets/editor/index.tsx',
    gamefinder: './targets/gamefinder/index.tsx'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: ''
  },
  devtool: 'inline-source-map',
  // devServer: {
  //   contentBase: path.join(__dirname, 'dist'),
  //   compress: true,
  //   port: 8000,
  // },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
    alias: {
      constants:path.resolve(__dirname, 'data/constants.json'),
      "constants.json": path.resolve(__dirname, 'data/constants.json'),
    }
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: [ /node_modules/ ],
        use: 'awesome-typescript-loader'
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      },
      {
        test: /\.scss$/,
        use: [{
          loader: 'style-loader' // creates style nodes from JS strings
        }, {
          loader: 'css-loader' // translates CSS into CommonJS
        }, {
          loader: 'sass-loader' // compiles Sass to CSS
        }]
      },
      {
        test: /\.(wav|png|jpg|gif)$/,
        loader: 'file-loader',
        options: {
          name: '/assets/[name].[ext]'
        }
      }
    ]
  }
}
