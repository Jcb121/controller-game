import { Howl } from 'howler'
import * as observer from 'node-observer'

import * as _constants from '../../data/constants.json'
const constants: any = _constants
import * as _config from '../../data/config.json'
const config: any = _config

const stealthTrack = require('./POL-stealth-mode-long.wav')

export default class GameMusic extends Howl {
  constructor() {
    
    super({
      src: stealthTrack,
      loop: true,
      volume: config.soundLevels.musicWaiting,
    })

    observer.subscribe(this, constants.round.set, this.roundStarted.bind(this))
    observer.subscribe(this, constants.round.failed, this.roundEnded.bind(this))
    observer.subscribe(this, constants.round.completed, this.roundEnded.bind(this))
    observer.subscribe(this, constants.game.toggleSound, this.toggleSound.bind(this))

    if(window.localStorage.getItem('musicMuted') != 'true'){
      this.play()
    }
  }

  toggleSound(who, unmuted){
    if(unmuted){
      this.stop()
    } else {
      this.play()
    }
  }

  roundStarted() {
    this.volume(config.soundLevels.musicRound)
  }
  roundEnded() {
    this.volume(config.soundLevels.musicWaiting) 
  }
}