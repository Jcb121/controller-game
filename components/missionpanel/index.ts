import Game from './game'

export default function(){
  const canvas = document.getElementById('mission')
  const game = new Game(canvas)  
  function gameLoop() {
    requestAnimationFrame(gameLoop)
    game.draw()
  }
  gameLoop()
}


