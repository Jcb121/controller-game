import { renderedCaptain, renderedMission } from '../../interfaces/mission'

interface store {
  order: renderedMission
  roundNumber: number
  orderFails: number
  playerScore: number
  teamScore: number
  captains: Array<renderedCaptain>
}

export const store: store = {
  order: {
    sentence: '',
    targetTime: 0
  },
  roundNumber: 0, //val
  orderFails: 0, // val
  playerScore: 0, // val
  teamScore: 0, // val
  captains: [] // ref
}

const subs = {}

export function setState(state){
  Object.keys(state).map(key => {
    if(store[key] != state[key]){
      store[key] = state[key]
      if(subs[key]){
        subs[key].forEach(entity => entity.update())
      }
    }
  })

}

export function subscribe(listening, entity){
  if(!subs[listening]) subs[listening] = []
  subs[listening].push(entity)
}