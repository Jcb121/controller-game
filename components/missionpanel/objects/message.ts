import * as PIXI from 'pixi.js'

const TEXT = 'DEFAULT TEXT'
const STYLE = {
  fontFamily: 'Press Start 2P', fontSize: 32, fill: 'white'
}

export default class Message extends PIXI.Text {
  constructor(text = TEXT, style = STYLE) {
    super(text, {
      ...STYLE,
      ...style
    })
    this.anchor.x = 0.5
    this.anchor.y = 0.5
  }

  setText(text = TEXT) {
    this.text = text
  }

  setX(value) {
    this.position.x = value
  }

  setY(value) {
    this.position.y = value
  }
}
