import Message from './message'
import { store, subscribe } from '../state'

export default class GameOverText extends Message{
  constructor(score = 'x'){
    super(`Team Score: ${score}`)
    this.position.x = 512
    this.position.y = 300
    subscribe('teamScore', this)
  }
  
  update(){
    this.text = `Team Score: ${store.teamScore}`
  }
}