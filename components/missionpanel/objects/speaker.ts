import * as PIXI from 'pixi.js'
import * as observer from 'node-observer'
import * as _constants from '../../../data/constants.json'
const constants: any = _constants

const soundIcon = require ('./sound-icon.png')
const soundIcon2 = require ( './sound-icon-2.png')


export default class Speaker extends PIXI.Sprite {
  muted: boolean
  persistent: boolean
  
  constructor(){
    super()
    this.persistent = true
    this.interactive = true
    this.on('pointerdown', this.toggleSound)

    this.height = 50
    this.width = 50

    this.x = 15
    this.y = 15
    
    this.muted = (window.localStorage.getItem('musicMuted') == 'true')
    this.setTexture()
  }

  setTexture(){
    if(this.muted === true){
      this.texture = PIXI.Texture.fromImage(soundIcon2)
    } else {
      this.texture = PIXI.Texture.fromImage(soundIcon)
    }
  }

  toggleSound(){
    this.muted = !this.muted
    this.setTexture()
    window.localStorage.setItem('musicMuted', this.muted? 'trtue' : 'false')
    observer.send(this, constants.game.toggleSound, this.muted)
  }
}  