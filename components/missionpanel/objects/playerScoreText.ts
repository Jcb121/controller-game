import Message from './message'
import { store, subscribe } from '../state'

export default class GameOverText extends Message{
  constructor(score = 'x'){
    super(`Your Score: ${score}`)
    this.position.x = 512
    this.position.y = 250
    subscribe('playerScore', this)
  }

  update(){
    this.text = `Your Score: ${store.playerScore}`
  }
}