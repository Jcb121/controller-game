export default class Throttle {

  state: boolean
  delay: number
  currentTime: number

  constructor(delay = 200, initialDelay = false) {
    this.state = initialDelay
    this.delay = delay
    this.currentTime = 0
  }

  update(time) {
    this.currentTime += time
    if (this.currentTime > this.delay || this.state == true) {
      this.state = true
    } else {
      this.state = false
    }
  }

  ready(time = false) {

    if (time) {
      this.update(time)
    }

    if (this.state) {
      this.state = false
      this.currentTime = 0
      return true
    } else {
      return false
    }
  }
}