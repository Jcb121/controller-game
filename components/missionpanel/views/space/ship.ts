import * as PIXI from 'pixi.js'
import * as observer from 'node-observer'
import * as _constants from '../../../../data/constants.json'
const constants: any = _constants

const shipPng = require('./ship.png')

const target_x = 1024 / 2
const target_y = 600 / 2
const target_z = 6
const target_bearing = 0

export default class Ship extends PIXI.Sprite {

  xVelocity: number
  yVelocity: number
  zVelocity: number
  rVelocity: number
  z: number

  constructor() {
    super()

    this.xVelocity = 0
    this.yVelocity = 0
    this.zVelocity = 0
    this.rVelocity = 0
    this.texture = PIXI.Texture.fromImage(shipPng)
    this.anchor.set(0.5, 0.5)
    this.x = 512 + 50
    this.y = 300 + 50
    this.z = 8

    observer.subscribe(this, constants.order.completed, this.orderCompleted.bind(this))
    observer.subscribe(this, constants.order.failed, this.orderFailed.bind(this))

    const ticker = new PIXI.ticker.Ticker()
    ticker.add(this.tick.bind(this))
    ticker.start()
  }

  tick(delta) {
    if (this.x > target_x) {
      this.apply_force(-0.1, 0)
    } else {
      this.apply_force(0.1, 0)
    }

    if (this.y > target_y) {
      this.apply_force(0, -0.1)
    } else {
      this.apply_force(0, 0.1)
    }

    if (this.z > target_z) {
      this.apply_force(0, 0, -0.001)
    } else {
      this.apply_force(0, 0, 0.001)
    }

    if (this.rotation > target_bearing) {
      this.apply_force(0, 0, 0, -0.001)
    } else {
      this.apply_force(0, 0, 0, 0.001)
    }

    this.x += this.xVelocity * delta
    this.y += this.yVelocity * delta
    this.z += this.zVelocity * delta
    this.scale.x = this.z
    this.scale.y = this.z
    this.rotation += this.rVelocity * delta
  }

  orderCompleted() {
    this.apply_force(0, 0, 0.2)
    setTimeout(() => {
      this.apply_force(0, 0, -0.2)
    }, 1000)
  }

  orderFailed() {

  }

  apply_force(force_x = 0, force_y = 0, force_z = 0, force_r = 0) {
    this.xVelocity += force_x
    this.yVelocity += force_y
    this.zVelocity += force_z
    this.rVelocity += force_r
  }
  draw() {

  }
}