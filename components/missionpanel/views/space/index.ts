import Ship from './ship'
import Star from './star'
import * as PIXI from 'pixi.js'

export default class Space extends PIXI.Container{
  constructor(){
    super()
    Array(50).fill(true).forEach(() => {
      this.addChild(new Star())
    })

    this.addChild(new Ship())
  }
}