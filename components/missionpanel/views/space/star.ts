import * as PIXI from 'pixi.js'
import * as observer from 'node-observer'
import * as _constants from '../../../../data/constants.json'
const constants: any = _constants

const onePx = require('./1x1.png')
const threePx = require('./3x3.png')
const fivePx = require('./5x5.png')
const options = [onePx, threePx, fivePx]
const overScan = 100

export default class Star extends PIXI.Sprite {
  xVelocity: number
  yVelocity: number

  constructor(){
    super()

    this.xVelocity = -Math.ceil(Math.random()) * 5
    this.yVelocity = 0

    this.texture = PIXI.Texture.fromImage(options[Math.floor(Math.random() * options.length)])
    
    this.scale.x = 2
    this.x = Math.random() * (1024 + overScan)
    this.y = Math.random() * 600

    observer.subscribe(this, constants.order.completed, this.orderCompleted.bind(this))
    observer.subscribe(this, constants.order.failed, this.orderFailed.bind(this))
    
    const ticker = new PIXI.ticker.Ticker()
    ticker.add(this.tick.bind(this))
    ticker.start()
  }

  orderCompleted(){
    this.xVelocity -= 3
    setTimeout(() => {
      this.xVelocity += 2.5
    }, 1000)
  }

  orderFailed(){

  }

  tick(delta){
    this.x += this.xVelocity * delta
    this.y += this.yVelocity * delta

    while (this.x < 0 - overScan){
      this.x += 1024 + overScan
    }
    while (this.x > 1024 + overScan){
      this.x -= 1024 - overScan
    }
  }
  
  draw(){
    
  }
}