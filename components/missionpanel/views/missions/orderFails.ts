import Message from '../../objects/message'
import * as observer from 'node-observer'
import constants from '../../../../data/constants.json'
import text from '../../../../data/text.json'
import {store, subscribe} from '../../state'

export default class OrderFails extends Message {
  constructor() {
    super('')
    this.setX(1024 - this.width)
    this.setY(50)
    this.anchor.x = 1
    subscribe('orderFails', this)
  }

  update() {
    this.text = new Array(store.orderFails).fill('✖').join('');
    this.setX(1024 - this.width)    
  }
}
