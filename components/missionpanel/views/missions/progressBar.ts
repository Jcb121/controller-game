import * as PIXI from 'pixi.js'
import * as observer from 'node-observer'
import * as _constants from '../../../../data/constants.json'
const constants: any = _constants
import * as colorTransition from 'colortransition'
import { store, subscribe } from '../../state'

export default class ProgressBar extends PIXI.Graphics {
  rectangleWidth: number
  rectangleColor: string
  orderStart: number
  orderEnd: number

  constructor() {
    super()
    this.rectangleWidth = 0
    this.rectangleColor = '#00ff00'

    const ticker = new PIXI.ticker.Ticker()
    ticker.add(this.tick.bind(this))
    ticker.start()
    subscribe('order', this)
  }

  update(){
    console.log(this)
    this.orderStart = Date.now()
    this.orderEnd = store.order.targetTime
  }

  tick(delta) {
    let timeSpan = this.orderEnd - this.orderStart
    let timeSpent = Date.now() - this.orderStart
    let timePercent = timeSpent / timeSpan * 100

    if (timePercent > 100){
      timePercent = 100
    }
    this.rectangleColor = colorTransition('#00ff00', '#ff0000', timePercent)
    this.rectangleWidth = 1024 / 100 * timePercent
    this.draw()
  }

  draw() {
    if(this.rectangleColor){
      var str = this.rectangleColor,
        num = parseInt(str.substring(1), 16)
  
      this.clear()
      this.lineStyle(0)
      this.beginFill(num)
      this.drawRect(0, 575, this.rectangleWidth, 25)
      this.endFill()
    }
  }
}