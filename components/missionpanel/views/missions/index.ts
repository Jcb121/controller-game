import * as PIXI from 'pixi.js'
import MissionText from './missionText'
import ProgressBar from './progressBar'
import RoundText from './roundText'
import OrderFails from './orderFails'

export default class Missions extends PIXI.Container {
  constructor() {
    super()
    this.addChild(new MissionText())
    this.addChild(new ProgressBar())
    this.addChild(new RoundText())
    this.addChild(new OrderFails())
  }
}