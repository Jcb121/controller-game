import Message from '../../objects/message'
import * as observer from 'node-observer'
import {store, subscribe} from '../../state'

export default class MissionText extends Message {
  constructor() {
    super('MISSION TEXT!')
    this.setX(1024 / 2)
    this.setY(600 / 2)
    subscribe('order', this)
  }

  update() {
    this.text = store.order.sentence
  }
}