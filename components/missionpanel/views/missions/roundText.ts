import Message from '../../objects/message'
import * as observer from 'node-observer'
import constants from '../../../../data/constants.json'
import * as _text from '../../../../data/text.json'
const text:any = _text
import {store, subscribe} from '../../state'

export default class RoundText extends Message{
  constructor() {
    super('MISSION TEXT!')
    this.setX(512)
    this.setY(50)
    subscribe('roundNumber', this)
  }

  update() {
    this.text = text.round.label + store.roundNumber
  }
}
