import * as PIXI from 'pixi.js'
import * as observer from 'node-observer'
import * as _constants from '../../../../data/constants.json'
const constants: any = _constants
import Title from './title'
import Ready from './ready'
import Credits from './credits'

const loadingPng = require('./loading.png')

export default class Waiting extends PIXI.Container{
  constructor(){
    super()
    observer.subscribe(this, constants.serial.setIds, this.readyUp.bind(this))
    let border = PIXI.Sprite.fromImage(loadingPng)
    this.addChild(border)
    this.addChild(new Credits())
    this.addChild(new Title())
  }
  readyUp(){
    this.addChild(new Ready())
  }
}