import Message from '../../objects/message'
import text from '../../../../data/text.json'

export default class Title extends Message{
  constructor() {
    super('Ready! Press Start!')
    this.setX(1024 / 2)
    this.setY(600 / 2)
  }

  update() {

  }

  draw() {

  }
}