import Message from '../../objects/message'
import * as _text from '../../../../data/text.json'
const text: any = _text

export default class Title extends Message{
  constructor(){
    super(text.game.title)
    this.setX(1024 / 2)
    this.setY(this.height)
  }
  
  update(){
  
  }

  draw(){

  }
}