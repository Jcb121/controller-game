import Message from '../../objects/message'
import Throttle from '../../objects/throttle'
import * as _text from '../../../../data/text.json'
const text: any = _text
import * as observer from 'node-observer'
import * as _constants from '../../../../data/constants.json'
const constants: any = _constants

export default class Credits extends Message{

  debounce: Throttle

  constructor() {
    super(`${text.waiting.credits}0`)
    this.debounce = new Throttle(60)
    // TODO: Replace this with more robust call
    observer.subscribe(this, constants.serial.setIds, this.readyUp.bind(this))
    this.setX(512)
    this.setY(600 - this.height)

    const ticker = new PIXI.ticker.Ticker()
    ticker.add(this.tick.bind(this))
    ticker.start()
  }

  readyUp() {
    this.text = `${text.waiting.credits}1`
  }

  tick(delta) {
    if (this.debounce.ready(delta)) {
      this.alpha = this.alpha ? 0 : 1
    }
  }
}

