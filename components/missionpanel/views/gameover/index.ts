import * as PIXI from 'pixi.js'

import EnterName from './enterName'
import GameOverText from './gameOverText'
import TeamScoreText from '../../objects/teamScoreText'
import PlayerScoreText from '../../objects/playerScoreText'

import * as _constants from '../../../../data/constants.json'
const constants: any = _constants
import * as observer from 'node-observer'

export default class GameOver extends PIXI.Container {
  constructor(){
    super()
    this.addChild(new GameOverText())
    this.addChild(new EnterName())
    this.addChild(new TeamScoreText())
    this.addChild(new PlayerScoreText())
    observer.subscribe(this, constants.game.nameEntered, this.nameEntered.bind(this))
  }
  
  nameEntered(who, name){
    // Send to server?
  }
}