import Message from '../../objects/message'
import Throttle from '../../objects/throttle'
import * as observer from 'node-observer'
const constants = require('../../../../data/constants.json')
import * as PIXI from 'pixi.js'

const asciiStart = 65
const asciiEnd = 90
const LETTERS = ['_', ...Array(asciiEnd - asciiStart + 1).fill(true).map((a, index) => {
  return String.fromCharCode(index + asciiStart)
})]

const letterWidth = 30

export default class EnterName extends PIXI.Container {
  childIndexPosition: number
  playerName: string
  children: Array<NameEntry>

  constructor() {
    super()
    this.childIndexPosition = 0
    this.playerName = ''
    this.addChild(new NameEntry(472))
    // Only subscribe when the active state...  
    observer.subscribe(this, constants.player.buttonPress, this.buttonPress.bind(this))
  }

  buttonPress(who, buttonPressed){
    if(who.type !== 'JOYSTICK'){
      return
    }

    if(buttonPressed === 'X'){
      this.playerName += this.children[this.childIndexPosition].letterSelected()
      if(this.playerName.length > 2){
        this.nameComplete()
      } else {
        this.childIndexPosition++
        this.addChild(new NameEntry(472 + this.childIndexPosition * letterWidth))    
      }
    } else {
      this.children[this.childIndexPosition].change(buttonPressed)   
    }
  }

  nameComplete(){
    observer.send(this, constants.game.nameEntered, this.playerName)
  }
}

class NameEntry extends Message {

  letterIndex: number
  debounce: Throttle
  flashing: boolean

  constructor(xPosition){
    super(LETTERS[0])
    this.letterIndex = 0
    this.debounce = new Throttle(0.8)
    this.setX(xPosition)
    this.setY(400)
    this.flashing = true
  }

  change(direction){
    if(direction === 'U' && this.letterIndex < LETTERS.length -1){
      this.letterIndex++
    } else if (direction === 'D' && this.letterIndex > 0){
      this.letterIndex--
    }
    this.text = LETTERS[this.letterIndex]
    this.alpha = 1        
  }

  letterSelected(){
    this.flashing = false
    this.alpha = 1
    return LETTERS[this.letterIndex]
  }

  update(delta){
    if (this.debounce.ready(delta) && this.flashing) {
      this.alpha = this.alpha ? 0 : 1
    }
  }
}