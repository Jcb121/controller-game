import Message from '../../objects/message'

export default class GameOverText extends Message {
  constructor() {
    super()
    this.text = 'Game Over'
    this.position.x = 512
    this.position.y = 200
  }
}