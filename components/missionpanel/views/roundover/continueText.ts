import Message from '../../objects/message'

export default class ConintueText extends Message {
  constructor() {
    super()
    this.text = 'Press Start to continue!'
    this.position.x = 512
    this.position.y = 400
  }
}