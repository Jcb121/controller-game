import Message from '../../objects/message'

export default class RoundOverText extends Message {
  constructor() {
    super()
    this.text = 'Round Complete!'
    this.position.x = 512
    this.position.y = 200
  }
}