import * as PIXI from 'pixi.js'

import RoundOverText from './roundOverText'
import ContinueText from './continueText'

import TeamScoreText from '../../objects/teamScoreText'
import PlayerScoreText from '../../objects/playerScoreText'

import constants from '../../../../data/constants.json'
import * as observer from 'node-observer'

import {store, subscribe} from '../../state'

export default class RoundOver extends PIXI.Container {
  constructor(){
    super()
    this.addChild(new RoundOverText())
    this.addChild(new ContinueText())
    this.addChild(new TeamScoreText())
    this.addChild(new PlayerScoreText() )
  }
}