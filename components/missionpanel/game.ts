import * as PIXI from 'pixi.js'
import * as observer from 'node-observer'
import * as _constants from '../../data/constants.json'
const constants: any = _constants


PIXI.settings.RESOLUTION = 2

import Waiting from './views/waiting'
import Missions from './views/missions'
import Space from './views/space'
import GameOver from './views/gameover'
import RoundOver from './views/roundover'
import Speaker from './objects/speaker'
import { store, setState } from './state'

export default class Game extends PIXI.Container {
  mode: Array<any>
  renderer: any

  constructor(view) {  
    super()
    this.renderer = PIXI.autoDetectRenderer(1024, 600, { view })

    this.mode = []

    this.addChild(new Space())
    this.addChild(new Waiting())
    this.addChild(new Missions())
    this.addChild(new RoundOver())
    this.addChild(new GameOver())
    this.addChild(new Speaker())

    observer.subscribe(this, constants.game.updateStage, this.updateState.bind(this))
    observer.subscribe(this, constants.game.changeStage, this.changeStage.bind(this))
  }

  updateState(who, newState) {
    setState(newState)
  }

  changeStage(who, payload = ['Space']) {
    this.mode = []
    this.children.forEach(child => {
      if (payload.indexOf(child.constructor.name) > -1 ) {
        child.visible = true
        this.mode.push(child)
      } else {
        child.visible = false
      }
    })
  }

  draw() {
    this.renderer.render(this)
  }
}