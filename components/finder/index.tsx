import * as React from 'react'

interface rootstate {
  state: string
}

export default class GameFinder extends React.Component<any, rootstate> {
  constructor(props){
    super(props);
    this.state = {
      state: ''
    }
  }
  onRoomNameChange(e){
    this.setState({state: e.target.value})
  }
  render(){
    return (
      <div>
        <h1>Controller-Game</h1>
        <p>Welcome to the cooperative online</p>

        <h2>Start a new game</h2>

        <input type="text" 
          value={this.state.state} 
          onChange={this.onRoomNameChange.bind(this)}/>
        {this.state.state && 
          <a href={`/rooms/${this.state.state}`}>Start</a>
        }

        <h2>Find a game:</h2>

        <a href="/">Refresh</a>

        {this.props.rooms.map(room => {
          return (
            <div key={room.name}>
              Rooms: {room.name} - {room.players}
              <a href={`/room/${room.name}`}>Join Game</a>
            </div>
          )
        })}
      </div>
    )
  }
}