import * as React from 'react'
import Module from './components/module'
import * as _constants from 'constants.json'
const constants: any = _constants;
const observer = require('node-observer')
import { player } from '../../interfaces/player'


interface root {
  player: player
}

interface rootState {
  order: boolean,
  score: number,
  names: any
}

export default class WebController extends React.Component<root, rootState> {
  player: player

  constructor(props: root) {
    super(props)
    this.state = {
      order: false,
      score: 0,
      names: {}
    }

    this.player = props.player
    observer.subscribe(this, constants.module.rename, this.renameModule.bind(this))
  }

  renameModule(who, { id, name }) {
    console.log('NAMES', this.state.names)
    let names = {... this.state.names}
    names[id] = name
    this.setState({ names })
  }

  render() {
    return <div ref="modulesWrapper" className="modules" style={this.player.layout}>
      {this.player.modules.map(module => {
        return <Module key={module.id} module={module} player={this.player} name={this.state.names[module.id]}/>
      })}
    </div>
  }
}