import * as React from 'react'
import ElementBase from '../elementBase'
import * as _constants from '../../../../../data/constants.json'
const constants: any = _constants;
export default class Radio extends ElementBase {

  async handleChange(groupKey, elementKey) {
    // this.soundEffect.play()
    let state = this.state.state
    state[groupKey] = elementKey
    await this.setState({ state })
    this.sendEvent(constants.player.buttonPress, groupKey)
    this.sendEvent(constants.player.stateChange, this.state.state)
  }

  render() {
    return this.props.module.capabilities.map(group => {
      return (
        <div key={group.key} className="capability" style={this.props.module.layout}>

          {this.props.module.capabilities.length > 1 &&
            <div className="radioLabel">
              {group.label}:
                </div>
          }

          {this.props.module.options.map(({ label, key }, index) => {
            return <div key={index} className="element">
              <label>
                <input
                  className="gameRadio"
                  type="radio"
                  name={this.props.module.id + group.label}
                  onChange={() => this.handleChange(group.key, key)}
                  value={key}
                  checked={this.state.state[group.key] === key}
                />
                <span>
                  {label}
                </span>
              </label>
            </div>
          })}
        </div>
      )
    })
  }
}