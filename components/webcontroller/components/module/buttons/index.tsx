import * as React from 'react'
import ElementBase from '../elementBase'
import * as _constants from '../../../../../data/constants.json'
const constants: any = _constants;

import { module } from '../../../../../interfaces/modules'

interface root {
  module: module
}


export default class Buttons extends ElementBase {
  async mouseDown(element) {
    // this.soundEffect.play()
    let state = {
      ...this.state.state,
      [element]: 1
    }
    await this.setState({ state })
    this.sendEvent(constants.player.buttonPress, element)
    this.sendEvent(constants.player.stateChange, this.state.state)
  }

  async mouseUp(element) {
    let state = {
      ...this.state.state,
      [element]: 0
    }
    await this.setState({ state })
    this.sendEvent(constants.player.stateChange, this.state.state)
  }

  render() {
    return this.props.module.capabilities.map((element, index) => {
      return <div key={index} className="element">
        <button
          className='gameButton'
          onMouseDown={this.mouseDown.bind(this, element.key)}
          onMouseUp={this.mouseUp.bind(this, element.key)}>
          {element.label}
        </button>
      </div>
    })

  }
}