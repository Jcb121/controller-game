import * as React from 'react'
import ElementBase from '../elementBase'
import * as _constants from '../../../../../data/constants.json'
const constants: any = _constants;

export default class Checkbox extends ElementBase {

  async handleChange(element, value) {
    // this.soundEffect.play()    
    value = value ? 1 : 0 // convert to binary
    let state = {
      ...this.state.state,
      [element]: value
    }
    await this.setState({ state })
    this.sendEvent(constants.player.buttonPress, element)
    this.sendEvent(constants.player.stateChange, this.state.state)
  }

  render() {
    return this.props.module.capabilities.map((element, index) => {
      return <div key={index} className="element">
        <label>
          <input type="checkbox"
            className="gameCheckbox"
            onChange={e => this.handleChange(element.key, e.target.checked)}
            defaultChecked={this.state.state[element.key]}
          />
          <span>
            {element.label}
          </span>
        </label>
      </div>
    })

  }
}