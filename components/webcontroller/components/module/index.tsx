import * as React from 'react'
import Radios from './radio'
import Checkboxs from './checkbox'
import Numbers from './number'
import Buttons from './buttons'

import { setupModule } from '../../../../interfaces/modules'
import { player } from '../../../../interfaces/player'

interface props {
  module: setupModule,
  name: string,
  player: player
}

export default class Module extends React.Component<props> {

  rightComponent() {
    switch (this.props.module.element) {
      case 'button':
        return <Buttons player={this.props.player} module={this.props.module} />
      case 'checkbox':
        return <Checkboxs player={this.props.player} module={this.props.module} />
      case 'number':
        return <Numbers player={this.props.player} module={this.props.module} />
      case 'radio':
        return <Radios player={this.props.player} module={this.props.module} />
    }
  }

  render() {

    let nameAble = 'name' in this.props.module

    console.log(this.props.module);
    let classes = `module ${this.props.module.type} ${nameAble ? 'nameAble' : ''}`
    return <div ref="container" className={classes} style={this.props.module.location}>

      {nameAble &&
        <h2 className="module__name">
          {this.props.name}&nbsp;
        </h2>
      }

      {this.rightComponent()}
    </div>
  }
}
