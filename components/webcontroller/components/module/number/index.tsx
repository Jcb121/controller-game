import * as React from 'react'
import ElementBase from '../elementBase'
import * as _constants from '../../../../../data/constants.json'
const constants: any = _constants;

const s = require('./styles.scss')
export default class Number extends ElementBase {

  upperBound: number

  componentWillReceiveProps(nextProps) {
    this.upperBound = parseInt(nextProps.module.options[1].key)
  }

  async handleChange(element, value) {
    // this.soundEffect.play()    
    let state = {
      ...this.state.state,
      [element]: value
    }
    await this.setState({ state })
    this.sendEvent(constants.player.buttonPress, element)
    this.sendEvent(constants.player.stateChange, this.state.state)
  }

  async increase(element) {
    // this.soundEffect.play()    
    let state = { ...this.state.state }
    let option = this.props.module.options.find(mod => mod.key == state[element.key])
    let index = this.props.module.options.indexOf(option)

    if (index < this.props.module.options.length - 1) {
      state[element.key] = this.props.module.options[index + 1].key
      await this.setState({ state })
      this.sendEvent(constants.player.buttonPress, element.key)
      this.sendEvent(constants.player.stateChange, this.state.state)
    }
  }

  async decrease(element) {
    // this.soundEffect.play()
    let state = { ...this.state.state }
    let option = this.props.module.options.find(mod => mod.key == state[element.key])
    let index = this.props.module.options.indexOf(option)

    if (index > 0) {
      state[element.key] = this.props.module.options[index - 1].key
      await this.setState({ state })
      this.sendEvent(constants.player.buttonPress, element.key)
      this.sendEvent(constants.player.stateChange, this.state.state)
    }
  }

  render() {
    return this.props.module.capabilities.map((element, index) => {
      return <div key={index} className="numberPicker">
        <div className="element">
          <button className="gameButton" onClick={() => this.increase(element)}>▲</button>
        </div>
        <span>
          { this.props.module.options.find(mod => mod.key == this.state.state[element.key]).label }
        </span>
        <div className="element">
          <button className="gameButton" onClick={() => this.decrease(element)}>▼</button>
        </div>
      </div>
    })

  }
}