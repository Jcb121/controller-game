import * as React from 'react'
import * as _constants from '../../../../data/constants.json'
const constants: any = _constants;
import * as observer from 'node-observer'
// import config from '../../../../data/config'

// webpack yes, serverside rendering no...
// import { Howl } from 'howler'
// import soundEffect from '../sounds/Arcade game tap button spring buzz_BLASTWAVEFX_30604.wav' 

import { setupModule } from '../../../../interfaces/modules'
import { player } from '../../../../interfaces/player'

interface root {
  module: setupModule,
  player: player
}

interface rootState {
  state: any
}

export default class elementBase extends React.Component<root, rootState> {
  constructor(props){
    super(props)

    // this.soundEffect = new Howl({
    //   src: soundEffect,
    //   volume: config.soundLevels.element,      
    // })   

    this.state = {
      state: props.module.state
    }
  }

  sendEvent(type, values) {

    if(type === constants.player.buttonPress){
      observer.send(this, type, values)
    }
    
    observer.send(this, constants.connection.send, {
      type,
      payload: {
        id: this.props.module.id,
        values
      }
    })
  }
}
