import * as React from 'react'
import { Route } from 'react-router'
import { Link } from 'react-router-dom'

import Layout from './layout'
import Missions from './missions'
import Devices from './devices'

const s = require('./styles.scss')
const sw = require('../webcontroller/styles.scss')

export default class Editor extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      layoutNames: [],
      player: {
      }
    }
  }

  render() {
    return (
      <div className="page">
        <div className="page__header">
          <h1>Controller-game Editor</h1>
          <Link to="/editor">Layout</Link>
          <Link to="/editor/missions">Missions</Link>
          <Link to="/editor/devices">Devices</Link>

        </div>
        <div className="page__content">
          <Route exact path='/editor' component={Layout} />
          <Route path='/editor/missions' component={Missions}/>
          <Route path='/editor/devices' component={Devices} />
        </div>
        <div className="page__footer">
          Made by Jesse Baker
        </div>
      </div>
    )
  }
}