import * as React from 'react'
import { Route } from 'react-router'
const s = require('./styles.scss')

import List from './components/list'
import DeviceEditor from './components/editor'
import Preview from './components/preview'

export default class Devices extends React.Component {
  render() {
    return (
      <div className="devices">
        <div>
          <h2>List</h2>
          <List />
        </div>
        <Route path='/editor/devices/:name' component={DeviceEditor} />
        <Route path='/editor/devices/:name' component={Preview} />
      </div>
    )
  }
}