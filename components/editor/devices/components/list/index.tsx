import * as React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import * as actions from './actions'

class List extends React.Component<any, any> {
  render(){
    return (
      <div>
        <ul>
          {this.props.devices.map((device, index) => {
            return (
              <li key={index}>
                {device.name}
                 - 
                {device.type}
                &nbsp;
                <button onClick={this.props.editDevice.bind(this, device)}>
                  Edit
                </button>
              </li>
            )
          })}
        </ul>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    devices: state.devices.devices
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(withRouter(List))
