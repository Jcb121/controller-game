export function editDevice(device) {    
    this.props.history.push(`/editor/devices/${device.name}`)    
    return {
        type: 'EDIT_DEVICE',
        payload: device
    }
  }