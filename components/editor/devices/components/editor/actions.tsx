export function addCapability(){
  return {
    type: 'ADD_NEW_DEVICE_CAPABILITY',
    payload: {
      key: 'e',
      label: 'Example'
    }
  }
}

export function addOption() {
  return {
    type: 'ADD_NEW_DEVICE_OPTION',
    payload: {
      key: 'e',
      label: 'Example'
    } 
  }
}

export function handleCapsChange(e, index, type){
  return {
    type: 'UPDATE_NEW_DEVICE_CAPABILITIES',
    payload: { value: e.target.value, index, type}
  }
}

export function deleteCap(index){
  return {
    type: 'DELETE_NEW_DEVICE_CAPABILITY',
    payload: index
  }
}
export function handleOptsChange(e, index, type){
  return {
    type: 'UPDATE_NEW_DEVICE_OPTIONS',
    payload: { value: e.target.value, index, type}
  }
}

export function deleteOpt(index){
  return {
    type: 'DELETE_NEW_DEVICE_OPTION',
    payload: index
  }
}

export function handleNameChange(e){
  return {
    type: 'UPDATE_NEW_DEVICE_NAME',
    payload: e.target.value
  }
}

export function saveDevice(){
  return function(dispatch, getState){
    let { device } = getState() 
    
    fetch('/devices', {
      method: 'POST',
      body: JSON.stringify(device),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
      .then(data => {
        dispatch({
          type: 'UPDATE_DEVICE',
          payload: device
        })
      })
  }
}

export function handleEmulatorChange(e){
  return {
    type: 'UPDATE_NEW_DEVICE_EMULATOR',
    payload: e.target.value
  }
}

export function handleNameableChange(e){
  return {
    type: 'UPDATE_NEW_DEVICE_NAMEABLE',
    payload: e.target.checked
  }
}

export function deleteDevice(){
  return function (dispatch, getState){
    let { device } = getState() 

    fetch(`/devices/${device.name}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
      .then(data => {
        dispatch({
          type: 'DELETE_DEVICE',
          payload: device.name
        })
      })
  }
}