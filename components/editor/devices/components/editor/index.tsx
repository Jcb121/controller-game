import * as React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import * as actions from './actions'

class DeviceEditor extends React.Component<any, any> {
  componentWillMount() {
    // this.props.editDevice(this.props.match.params.name)
  }
  componentWillReceiveProps(nextProps){
    nextProps.editDevice(nextProps.match.params.name)
  }
  
  render() {
    return (
      <div>

        <div>
          <label>
            Device Name:
            <input type="text" value={this.props.name} onChange={this.props.handleNameChange}/>
          </label>
        </div>

        <div>
          <label>
            emulated by:
            <select 
              value={this.props.emulator}
              onChange={this.props.handleEmulatorChange}>
              <option value="button">Button</option>
              <option value="radio">Radio</option>
              <option value="number">Number</option>
              <option value="checkbox">Checkbox</option>
            </select>
          </label>
        </div>

        <div>
          <label>
            Is nameable?
            <input type="checkbox" defaultChecked={this.props.isNameable} onChange={this.props.handleNameableChange}/>
          </label>
        </div>

        <h4>Capabilities</h4>
        <button onClick={this.props.addCapability}>+</button>
        {this.props.capabilities.map((cap, index) => {
          return (
            <div key={index}>
              <label>key:
                <input type="text" value={cap.key} 
                  onChange={e => this.props.handleCapsChange(e, index, 'key')}/>
              </label>
              <label>Label:
                <input type="text" value={cap.label} 
                  onChange={e => this.props.handleCapsChange(e, index, 'label')}/>
              </label>
              <button onClick={this.props.deleteCap.bind(this, index)}>X</button>
            </div>
          )
        })}

        <h4>Options</h4>
        <button onClick={this.props.addOption}>+</button>
        {this.props.options.map((opt, index) => {
          return (
            <div key={index}>
              <label>key:
                <input type="text" value={opt.key} 
                  onChange={e => this.props.handleOptsChange(e, index, 'key')}/>
              </label>
              <label>Label:
                <input type="text" value={opt.label} 
                  onChange={e => this.props.handleOptsChange(e, index, 'label')}/>
              </label>
              <button onClick={this.props.deleteOpt.bind(this, index)}>X</button>
            </div>
          )
        })}

        <button onClick={this.props.saveDevice}>Save</button>
        <Link 
          onClick={this.props.saveDevice}
          to="/editor/devices">
          Save and close</Link>
        <button onClick={this.props.deleteDevice}>Delete</button>

      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    name: state.device.name,
    capabilities: state.device.capabilities,
    options: state.device.options,
    emulator: state.device.emulator,
    isNameable: state.device.isNameable
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(DeviceEditor)
