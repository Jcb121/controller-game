import * as React from 'react'
import { connect } from 'react-redux'

import Module from '../../../../webcontroller/components/module'

// preview of a single device

class Preview extends React.Component<any, any> {

  render() {
    const state = this.props.capabilities.reduce((state, cap) => {
      state[cap.key] = this.props.options[0].key
      return state
    }, {})

    // const newModule = {
    //   id: '',
    //   
    //   
    //   location: {},
    //   name: 'Jezza',
    //   
    //   options: this.props.options,
    //   state,
    //   // isNameable: this.props.isNameable
    // }

    const newModule = {
      id: 'sample',
      capabilities: this.props.capabilities,
      type: this.props.type,
      element: this.props.emulator,
      location: {
        gridColumnStart: 1,
        gridColumnEnd: 2,
        gridRowStart: 1,
        gridRowEnd: 2,
        gridTemplateRows: '1fr',
        gridTemplateColumns: '1fr 1fr f1r'
      },
      options: [
        {
          key: '1',
          label: 'ass'
        }
      ],
      state: {

      }
    }

    return (
      <Module
        player={{
          layoutName: 'test',
          layout: {
            gridTemplateRows: '1fr',
            gridTemplateColumns: '1fr'
          },
          modules: [ newModule ]
        }}
        name='jezza'
        module={newModule} />
    )
  }
}

function mapStateToProps(state) {
  return {
    name: state.device.name,
    capabilities: state.device.capabilities,
    options: state.device.options,
    emulator: state.device.emulator,
    isNameable: state.device.isNameable
  }
}

export default connect(mapStateToProps)(Preview)
