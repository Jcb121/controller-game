import * as React from 'react'
import Preview from './components/preview/index'
import LayoutController from './components/layoutController/index'
import GridController from './components/gridController/index'
import ModuleController from './components/moduleController/index'

const s = require('./styles.scss')

export default class Layout extends React.Component {

  render(){
    return(
      <div className="editor">
        <div className="editor__left">
          <Preview></Preview>
        </div>
        <div className="editor__right">
          <div>
            <LayoutController></LayoutController>
            <GridController></GridController>
            <ModuleController></ModuleController>
          </div>
        </div>
      </div>
    )
  }
}