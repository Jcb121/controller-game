import * as React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

const s = require('./styles.scss')
import { layout } from '../../../../../interfaces/layouts';

interface gridLines {
  dispatch: Function,
  selected: [number, number]
  rows: string,
  columns: string,
  dragging: boolean,
  selectArea: Function,
  layout: layout
}

class GridLines extends React.Component<gridLines> {
  cellColor(x, y) {
    let [_x, _y] = this.props.selected
    if (_x === x && _y === y) {
      return 'green'
    } else if (_x === x || _y == y) {
      return 'red'
    } else {
      return ''
    }
  }

  dragOver(x, y, e) {
    this.props.dispatch({
      type: 'SELECT_AREA',
      payload: [x, y]
    })
  }

  render() {
    let rows = this.props.rows.split(' ')
    let columns = this.props.columns.split(' ')
    
    return (
      <div
        className="gridLines"
        style={{
          ...this.props.layout,
          zIndex: this.props.dragging ? 1 : -0 
        }}>
        {rows.map((width, y) => {
          return columns.map((height, x) => {
            return (
              <div
                key={`grid${x}${y}`}
                onDragOver={this.dragOver.bind(this, x, y)}
                onClick={() => this.props.selectArea(x, y)}
                className="gridLines__area"
                style={{
                  gridRowStart: y + 1,
                  gridColumnStart: x + 1,
                  outlineColor: this.cellColor(x, y)
                }}
              >{x + 1}, {y + 1}</div>
            )
          })
        })}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    rows: state.layouts.layout.layout.gridTemplateRows,
    columns: state.layouts.layout.layout.gridTemplateColumns,
    selected: state.layouts.selectedArea,
    layout: state.layouts.layout.layout,
    dragging: state.layouts.dragging
  };
}

export default connect(mapStateToProps)(GridLines);
