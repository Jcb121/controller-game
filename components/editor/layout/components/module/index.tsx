import * as React from 'react'
import Radios from '../../../../webcontroller/components/module/radio'
import Checkboxs from '../../../../webcontroller/components/module/checkbox'
import Numbers from '../../../../webcontroller/components/module/number'
import Buttons from '../../../../webcontroller/components/module/buttons'
import { connect } from 'react-redux'
import { setupModule } from '../../../../../interfaces/modules'
import { player } from '../../../../../interfaces/player'

interface root {
  module: setupModule,
  player: player,
  selected: [number, number],
  dispatch: Function,
  dragging: boolean,
  selectedModule: setupModule,
  name: string
}

interface rootState {
  style: any,
  dragging: boolean
}

const state: rootState = {
  style: {},
  dragging: false
}

class Module extends React.Component<root, rootState> {

  constructor(props) {
    super(props);
    this.state = state
  }

  rightComponent() {   
    switch (this.props.module.element) {
      case 'button':
        return <Buttons player={this.props.player} module={this.props.module} />
      case 'checkbox':
        return <Checkboxs player={this.props.player} module={this.props.module} />
      case 'number':
        return <Numbers player={this.props.player} module={this.props.module} />
      case 'radio':
        return <Radios player={this.props.player} module={this.props.module} />
    }
  }

  dragStart(e) {
    this.setState({ dragging: true })
    this.props.dispatch({ type: 'MODUDLE_DRAG', payload: true })
  }

  drag() {
    let { location } = this.props.module
    let height = location.gridRowEnd - location.gridRowStart
    let width = location.gridColumnEnd - location.gridColumnStart
    let [x, y] = this.props.selected

    this.setState({
      style: {
        ...this.state.style,
        gridRowStart: y + 1,
        gridRowEnd: y + 1 + height,
        gridColumnStart: x + 1,
        gridColumnEnd: x + 1 + width
      }
    })
  }

  dragEnd() {
    this.setState({ dragging: false })
    this.props.dispatch({ type: 'MODUDLE_DRAG', payload: false })

    let { location } = this.props.module
    let height = location.gridRowEnd - location.gridRowStart
    let width = location.gridColumnEnd - location.gridColumnStart
    let [x, y] = this.props.selected

    this.props.dispatch({
      type: 'UPDATE_SELECTED_MODULE', payload: {
        gridRowStart: y + 1,
        gridRowEnd: y + 1 + height,
        gridColumnStart: x + 1,
        gridColumnEnd: x + 1 + width
      }
    })
  }

  selectModule() {
    this.props.dispatch({
      type: 'MODULE_SELECTED',
      payload: this.props.module
    })
  }

  render() {

    if (this.props.module === this.props.selectedModule) {
      console.log(true)
    }

    return [
      this.state.dragging ? <div key="hover" className="hoverBox" style={this.state.style}></div> : null
      ,
      <div
        key="element"
        onClick={this.selectModule.bind(this)}
        ref="container"
        className={`module ${this.props.module.type}`}
        style={{
          ...this.props.module.location,
          gridTemplateRows: '1fr',
          gridTemplateColumns: '1fr'
        }}
      >

        <div
          style={{ ...this.props.module.location, display: 'grid', gridArea: '1 / 1' }}
          draggable={true}
          onDrag={this.drag.bind(this)}
          onDragEnd={this.dragEnd.bind(this)}
          onDragStart={this.dragStart.bind(this)}
        >

          {'name' in this.props.module &&
            <h2 className="module__name">
              {this.props.name}&nbsp;
        </h2>
          }

          {this.rightComponent()}
        </div>
      </div>
    ]
  }
}

function mapStateToProps(state) {
  return {
    selected: state.layouts.selectedArea,
    selectedModule: state.layouts.layout.modules[state.layouts.selectedModule],
    selectedModuleIndex: state.layouts.selectedModule
  };
}

export default connect(mapStateToProps)(Module);
