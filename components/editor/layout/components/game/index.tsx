import * as React from 'react'
import { connect } from 'react-redux'
import Module from '../module'
import * as observer from 'node-observer'
import { layout } from '../../../../../interfaces/layouts';


class Game extends React.Component<layout> {
  // constructor(props) {
  //   super(props)
  //   this.state = {
  //     player: {
  //       modules: []
  //     },
  //     order: false,
  //     score: 0,
  //     selected: [],
  //     layouts: [],
  //     layoutNames: props.layoutNames
  //   }
  // }
  // updatePlayerStyle(style) {
  //   this.setState({
  //     player: {
  //       ...this.state.player,
  //       style: {
  //         ...this.state.player.style,
  //         ...style
  //       }
  //     }
  //   })
  // }
  // updateModuleLocation(module, location) {
  //   let modules = [...this.state.player.modules]
  //   let mod = modules.find(mod => mod === module)
  //   mod.location = location
  //   this.setState({ modules })
  // }

  render() {

    let layout = this.props.layout;
    let modules = this.props.modules

    return <div className="modules" style={layout}>
      {modules.map((module, index) => {
        return <Module
          key={index}
          module={module}
        />
      })}
    </div>
  }
}

function mapStateToProps(state) {
  return {
    layout: state.layouts.layout,
  }
}

export default connect(mapStateToProps)(Game)
