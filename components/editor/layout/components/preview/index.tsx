import * as React from 'react'
import GridLines from  '../gridLines'
import Game from '../game'

const s = require('./styles.scss')

export default class Preview extends React.Component { 
  
  render(){
    return (
      <div className="preview">
        <div className="preview__wrapper">
          <div className="preview__panel">
              Game here
          </div>
          <div className="preview__controller">
            <GridLines></GridLines>
            <Game></Game>
          </div>
        </div>
      </div>
    )
  }
}