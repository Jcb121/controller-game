import * as React from 'react'
import { connect } from 'react-redux'
import { module } from '../../../../../interfaces/modules'
import { player, defaultPlayer } from '../../../../../interfaces/player'

interface moduleController {
  updateLocation: Function,
  deleteModule: Function,
  module: module
}

interface moduleControllerState {
  player: player
}

class ModuleController extends React.Component<moduleController, moduleControllerState> {

  constructor(props){
    super(props)
    this.state = {
      player: defaultPlayer
    } 
  }

  insertModule(type) {
    
  }

  deleteModule(module) {
    let player = {
      ...this.state.player,
      modules: this.state.player.modules.filter(mod => mod !== module)
    }
    this.setState({ player })
  }

  moveLeft(){
    let location = {...this.props.module.location}
    location.gridColumnStart--
    location.gridColumnEnd--
    this.props.updateLocation(this.props.module, location)
  }

  moveRight(){
    let location = {...this.props.module.location}
    location.gridColumnStart++
    location.gridColumnEnd++
    this.props.updateLocation(this.props.module, location)
  }

  moveUp(){
    let location = {...this.props.module.location}
    location.gridRowStart--
    location.gridRowEnd--
    this.props.updateLocation(this.props.module, location)
  }

  moveDown(){
    let location = {...this.props.module.location}
    location.gridRowStart++
    location.gridRowEnd++
    this.props.updateLocation(this.props.module, location)
  }

  increaseHeight(){
    let location = {...this.props.module.location}
    location.gridRowEnd++
    this.props.updateLocation(this.props.module, location)
  }
  
  decreaseHeight(){
    let location = {...this.props.module.location}
    location.gridRowEnd--
    this.props.updateLocation(this.props.module, location)
  }
  
  increaseWidth(){
    let location = {...this.props.module.location}
    location.gridColumnEnd++
    this.props.updateLocation(this.props.module, location)
  }

  decreaseWidth(){
    let location = {...this.props.module.location}
    location.gridColumnEnd--
    this.props.updateLocation(this.props.module, location)
  }

  delete(){
    this.props.deleteModule(this.props.module)
  }

  render(){
    return (
      <div className="moduleController">
        <h2>Module Controls</h2>
        {this.props.module && 
          <h3>{this.props.module.type}</h3>
        }
        <button className="moduleController__moveUp" onClick={this.moveUp.bind(this)}>▲</button>
        <button className="moduleController__moveLeft" onClick={this.moveLeft.bind(this)}>◀</button>
        <button className="moduleController__moveDown" onClick={this.moveDown.bind(this)}>▼</button>
        <button className="moduleController__moveRight" onClick={this.moveRight.bind(this)}>▶</button>
        <button className="moduleController__increaseWidth" onClick={this.increaseWidth.bind(this)}>W +</button>
        <button className="moduleController__decreaseWidth" onClick={this.decreaseWidth.bind(this)}>W -</button>
        <button className="moduleController__increaseHeight" onClick={this.increaseHeight.bind(this)}>H +</button>
        <button className="moduleController__decreaseHeight" onClick={this.decreaseHeight.bind(this)}>H -</button>
        <button className="moduleController__delete" onClick={this.delete.bind(this)}>X</button>
    </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    layout: state.layouts.layout,
    selectedModule: state.layouts.selectedModule,
    modules: state.layouts.layout.modules,
    module: state.layouts.layout.modules[state.layouts.selectedModule]
  };
}

export default connect(mapStateToProps)(ModuleController);
