import * as React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from './actions'
import { player } from '../../../../../interfaces/player'
import { layout } from '../../../../../interfaces/layouts';

// sets up the player state, this kinda sucks in this implementation.
function setupPlayer(player) {
  player.modules = player.modules.map(module => {
    if ('name' in module) module.name = 'Jezza'

    if (typeof module.options === 'object') {
      module.state = module.capabilities.reduce((state, { key }) => {
        state[key] = module.options[0].key
        return state
      }, {})
    } else if (module.element === 'checkbox' || module.element === 'number') {
      let upper = parseInt(module.options.split('-')[1])
      let options = Array(upper + 1).fill('a').map((a: string, index: number) => index)
      module.state = module.capabilities.reduce((state, { key }) => {
        let random = Math.floor(Math.random() * options.length)
        state[key] = options[random]
        return state
      }, {})
    } else {
      module.state = module.capabilities.reduce((state, element) => {
        state[element.key] = 0
        return state
      }, {})
    }
    return module
  })
  return player
}

interface root {
  layoutName: string,
  layoutNames: Array<string>
  getLayoutNames: Function,
  updateLayout: Function,

}
interface rootState {
  rows: Array<string>,
  columns: Array<string>
  player: player,
  layoutNames: Array<string>
}

class LayoutController extends React.Component<root, rootState> {
  save() {
    let layout = {
      gridTemplateRows: this.state.rows.join(' '),
      gridTemplateColumns: this.state.columns.join(' ')
    }
    let modules = this.state.player.modules.map(module => {
      let mod = { ...module }
      delete mod.state
      if (mod.name) mod.name = ''
      return mod
    })

    let layoutName = this.state.player.layoutName
    let existingProfile = this.state.layoutNames.find((layout: string) => layout == layoutName)

    if (!existingProfile) {
      this.setState({
        layoutNames: [
          ...this.state.layoutNames,
          layoutName
        ]
      })
    }

    fetch('/layouts', {
      method: 'POST',
      body: JSON.stringify({ layoutName, layout, modules }),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
      .then(data => {
        console.log(data)
      })
  }

  delete() {
    let layoutName = this.state.player.layoutName
    fetch(`/layouts/${layoutName}`, {
      method: 'delete',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
      .then(data => {
        console.log(data)
        let layoutNames = this.state.layoutNames.filter(layout => layout != layoutName)
        
        this.setState({
          layoutNames, 
          player: {
            layoutName: '',
            modules: [],
            layout: {
              gridTemplateRows: 'auto',
              gridTemplateColumns: 'auto'
            }
          }
        })
      })
  }

  layoutNameChange(event) {
    this.props.updateLayout({
      layoutName: event.target.value
    })
  }

  changeProfile(event) {
    fetch(`/layouts/${event.target.value}`).then(res => res.json())
      .then(layout => {
        console.log(layout)
        this.props.updateLayout(setupPlayer(layout))
      })
  }

  getLayoutNames(){
    fetch('/layouts').then(response => response.json())
      .then(layoutNames => {
        this.props.getLayoutNames(layoutNames)
      })
  }

  render() {
    return (
      <div>
        <h2>Layout Controls</h2>
        <h3>Working</h3>
        <button onClick={this.getLayoutNames.bind(this)} >Refresh</button>
        <label>
          Layout:
          <select value={this.props.layoutName} onChange={this.changeProfile.bind(this)}>
            <option>none</option>
            {this.props.layoutNames.map(name => {
              return <option value={name} key={name}>{name}</option>
            })}
          </select>
        </label>
        <label>
          Name:
          <input type="text" value={this.props.layoutName} onChange={this.layoutNameChange.bind(this)} />
        </label>

        <h3>Not Working</h3>
        
        <button onClick={this.save.bind(this)}>Save</button>
        <button onClick={this.delete.bind(this)}>Delete</button>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    layoutNames: state.layouts.layoutNames,
    layoutName: state.layouts.layout.layoutName
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(LayoutController)
