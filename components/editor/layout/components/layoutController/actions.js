export function getLayoutNames(names) {
  return {
    type: 'UPDATE_NAMES',
    payload: names
  }
}

export function updateLayout(layout){
  return {
    type: 'UPDATE_LAYOUT',
    payload: layout
  }
}