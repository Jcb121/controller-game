import * as React from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './actions'

interface GridController {
  increaseRows: Function,
  decreaseRows: Function,
  increaseColumns: Function,
  decreaseColumns: Function,
  increaseHeight: Function,
  decreaseHeight: Function,
  increaseWidth: Function,
  decreaseWidth: Function
}

class GridController extends React.Component<GridController> {
  render(){
    return (
      <div>
        <h2>Grid Controls</h2>
        <button onClick={this.props.increaseRows.bind(this)}>Rows +</button>
        <button onClick={this.props.decreaseRows.bind(this)}>Rows -</button>
        <button onClick={this.props.increaseColumns.bind(this)}>Cols +</button>
        <button onClick={this.props.decreaseColumns.bind(this)}>Cols -</button>

        <button onClick={this.props.increaseHeight.bind(this)}>Height +</button>
        <button onClick={this.props.decreaseHeight.bind(this)}>Height -</button>
        <button onClick={this.props.increaseWidth.bind(this)}>Width +</button>
        <button onClick={this.props.decreaseWidth.bind(this)}>Width -</button>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    layout: state.layouts.layout,
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(GridController);
