import { combineReducers } from 'redux'

import gridLayout from './gridLayout'
import layouts from './layouts'
import player from './player'
import devices from './devices'
import device from './device'
import missions from './missions'
import mission from './mission'

const allReducers = combineReducers({
  gridLayout,
  layouts,
  player,
  devices,
  device,
  missions,
  mission
})

export default allReducers