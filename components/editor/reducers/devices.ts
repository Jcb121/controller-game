export const DEFAULT_STATE = {
  devices: []
}

export default function (state = DEFAULT_STATE, action) {
  var devices
  var index
  var device
  switch (action.type) {
  case 'DELETE_DEVICE': 
    return { 
      ...state,
      devices: state.devices.filter(d => d.name != action.payload) 
    }
  case 'UPDATE_DEVICE':
    devices = [...state.devices]
    device = devices.find(device => device.name == action.payload.name)  

    if(device) {
      devices[devices.indexOf(device)] = action.payload
      return {
        ...state,
        devices
      }
    } else {
      return {
        ...state,
        devices: [...state.devices, action.payload]
      }
    }
  }
  return state
}