export const DEFAULT_STATE = {
  name: 'example',
  emulator: 'button',
  isNameable: true,
  options: [
    { key: '0', label: 'Off' },
    { key: '1', label: 'On' }
  ],
  capabilities: [
    { key: '1', label: 'Example' }
  ],
  prevOptions: [
    { key: '0', label: 'A' },
    { key: '1', label: 'B' }
  ]
}

export default function (state = DEFAULT_STATE, action) {
  switch (action.type) {
  case 'ADD_NEW_DEVICE_CAPABILITY':
    return {
      ...state,
      capabilities: [
        ...state.capabilities,
        action.payload
      ]
    }
  case 'ADD_NEW_DEVICE_OPTION':
    return {
      ...state,
      options: [
        ...state.options,
        action.payload
      ]
    }
  case 'UPDATE_NEW_DEVICE_CAPABILITIES':
    var cap = state.capabilities[action.payload.index]
    cap[action.payload.type] = action.payload.value
    return {
      ...state,
      capabilities: [...state.capabilities]
    }
  case 'DELETE_NEW_DEVICE_CAPABILITY':
    state.capabilities.splice(action.payload, 1)
    return {
      ...state,
      capabilities: [...state.capabilities]
    }
  case 'UPDATE_NEW_DEVICE_OPTIONS':
    var opt = state.options[action.payload.index]
    opt[action.payload.type] = action.payload.value
    return {
      ...state,
      options: [...state.options]
    }
  case 'DELETE_NEW_DEVICE_OPTION':
    state.options.splice(action.payload, 1)
    return {
      ...state,
      options: [...state.options]
    }
  case 'UPDATE_NEW_DEVICE_NAME':
    return {
      ...state,
      name: action.payload
    }
  case 'UPDATE_NEW_DEVICE_EMULATOR':
    var options, prevOptions
    if (action.payload == 'checkbox' || action.payload == 'button') {
      prevOptions = [...state.options]
      options = [
        { key: '0', label: 'Off' },
        { key: '1', label: 'On' }
      ]
    } else {
      options = state.prevOptions
    }

    return {
      ...state,
      emulator: action.payload,
      options,
      prevOptions
    }
  case 'UPDATE_NEW_DEVICE_NAMEABLE':
    return {
      ...state,
      isNameable: action.payload
    }
  case 'EDIT_DEVICE':
    return {
      ...DEFAULT_STATE,
      ...action.payload
    }
  }
  return state
}