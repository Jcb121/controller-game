import { module } from "../../../interfaces/modules";


interface playerLayout {
  layout: any,
  layoutName: string,
  modules: Array<module>
}
interface defaultState {
  layoutNames: string[],
  dragging: boolean,
  layout: playerLayout,
  selectedArea: [number, number],
  selectedModule: number
}

const DEFAULT_STATE: defaultState = {
  layoutNames: [],
  dragging: false,
  layout: {
    layoutName: '',
    layout: {
      gridTemplateColumns: '1fr',
      gridTemplateRows: '1fr'
    },
    modules: []
  },
  selectedArea: [0, 0],
  selectedModule: 0
}

export default function (state = DEFAULT_STATE, action: any) {
  let gridTemplateRows, gridTemplateColumns
  switch (action.type) {
    case 'UPDATE_SELECTED_MODULE':
      return {}
    case 'MODULE_SELECTED':
      return {
        ...state,
        selectedModule: state.layout.modules.indexOf(action.payload)
      }
    case 'MODUDLE_DRAG':
      return {
        ...state,
        dragging: action.payload
      }
    case 'UPDATE_NAMES':
      return {
        ...state,
        layoutNames: action.payload
      }
    case 'UPDATE_LAYOUT':
    console.log(action.payload)
      return {
        ...state,
        layout: {
          ...state.layout,
          ...action.payload
        }
      }
    case 'SELECT_AREA':
      return {
        ...state,
        selectedArea: action.payload
      }
    case 'INCREASE_ROWS':
      gridTemplateRows = state.layout.layout.gridTemplateRows.split(' ');
      gridTemplateRows.push('1fr')
      gridTemplateRows = gridTemplateRows.join(' ')
      return {
        ...state,
        layout: {
          ...state.layout,
          layout: {
            ...state.layout.layout,
            gridTemplateRows
          }
        }
      }
    case 'DECREASE_ROWS':
      gridTemplateRows = state.layout.layout.gridTemplateRows.split(' ');
      gridTemplateRows.pop()
      gridTemplateRows = gridTemplateRows.join(' ')
      return {
        ...state,
        layout: {
          ...state.layout,
          layout: {
            ...state.layout.layout,
            gridTemplateRows
          }
        }
      }
    case 'INCREASE_COLUMNS':
      gridTemplateColumns = state.layout.layout.gridTemplateColumns.split(' ');
      gridTemplateColumns.push('1fr')
      gridTemplateColumns = gridTemplateColumns.join(' ')
      return {
        ...state,
        layout: {
          ...state.layout,
          layout: {
            ...state.layout.layout,
            gridTemplateColumns
          }
        }
      }
    case 'DECREASE_COLUMNS':
      gridTemplateColumns = state.layout.layout.gridTemplateColumns.split(' ');
      gridTemplateColumns.pop()
      gridTemplateColumns = gridTemplateColumns.join(' ')
      return {
        ...state,
        layout: {
          ...state.layout,
          layout: {
            ...state.layout.layout,
            gridTemplateColumns
          }
        }
      }
  }
  return state;
}