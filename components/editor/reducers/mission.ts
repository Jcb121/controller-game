export const DEFAULT_STATE = {
  name: '',
  targets: [],
  length: 3,
  condition: '',
  type: 'event',
  sentences: [],
  sentence: ''
}

export default function (state = DEFAULT_STATE, action) {
  switch (action.type) {
  case 'UPDATE_NEW_MISSION_NAME':
    return {
      ...state,
      name: action.payload
    }  
  case 'ADD_NEW_MISSION_TARGET':
    if(state.targets.indexOf(action.payload) > -1){
      return state
    }
    return {
      ...state,
      targets: [...state.targets, action.payload]
    }
  case 'REMOVE_NEW_MISSION_TARGET':    
    var targets = state.targets.filter(t => t != action.payload)
    return {
      ...state,
      targets
    }
  case 'UPDATE_NEW_MISSION_CONDITION':
    return {
      ...state,
      condition: action.payload
    }
  case 'UPDATE_NEW_MISSION_TYPE':
    return {
      ...state,
      type: action.payload
    }
  case 'UPDATE_NEW_MISSION_LENGTH':
    return {
      ...state,
      length: action.payload
    }
  
  case 'DELETE_NEW_MISSION_SENTENCE':
    state.sentences.splice(state.sentences.indexOf(action.payload), 1)
    return {
      ...state,
      sentence: [...state.sentences]
    }
  case 'UPDATE_NEW_MISSION_SENTENCE':
    return {
      ...state,
      sentence: action.payload
    }
  case 'ADD_NEW_MISSION_SENTENCE':
    return {
      ...state,
      sentences: [...state.sentences, state.sentence],
      sentence: ''
    }
  case 'EDIT_MISSION':
    return {
      ...DEFAULT_STATE,
      ...action.payload
    }
  }
  return state
}