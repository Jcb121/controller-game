export const DEFAULT_STATE = {
  missions: []
}

export default function (state = DEFAULT_STATE, action ) {
  var index
  switch (action.type) {
  case 'DELETE_MISSION':
    return {
      ...state,
      missions: state.missions.filter(m => m.name != action.payload.name)
    }
  case 'UPDATE_MISSION':
    index = state.missions.findIndex(m => m.name == action.payload.name)
    if(index === -1){
      state.missions.push(action.payload)
      return { 
        ...state,
        missions: [ ...state.missions ]
      }
    } else {
      state.missions[index] = action.payload
      return {
        ...state,
        missions: [...state.missions]
      }    
    }
  }
  return state 
}