const DEFAULT_STATE = {
  layoutName: '',
}
export default function (state = DEFAULT_STATE, action) {
  switch (action.type) {
    case 'UPDATE_PLAYER':
      console.log(action.payload)
      return {
        ...state,
        ...action.payload
      }
  }
  return state;
}