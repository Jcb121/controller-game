import * as React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { mission } from '../../../../../interfaces/mission'

interface root {
  missions: Array<mission>
}

class MissionList extends React.Component<root>{
  render(){
    return (
      <div>
        <h1>Mission List</h1>
        <ul>
          {this.props.missions.map(mission => {
            return <li key={mission.name}>
              {mission.name}
              <Link to={`/editor/missions/${mission.name}`}>
                Edit
              </Link>
            </li>
          })}
        </ul>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    missions: state.missions.missions
  }
}

export default connect(mapStateToProps)(MissionList)
