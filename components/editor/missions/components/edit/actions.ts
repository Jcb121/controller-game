export function deleteMission(){
  return function(dispatch: Function, getState: Function){
    let { mission } = getState()

    fetch(`/missions/${mission.name}`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
      .then(data => {
        dispatch({
          type: 'DELETE_MISSION',
          payload: mission
        })
      })
  }
}

export function updateMission(){
  return function (dispatch: Function, getState: Function) {
    let { mission } = getState()
    
    fetch('/missions', {
      method: 'POST',
      body: JSON.stringify(mission),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(res => res.json())
      .then(data => {
        dispatch({
          type: 'UPDATE_MISSION',
          payload: mission
        })
      })
  }  
}

export function handleNameChange(e: any){
  return {
    type: 'UPDATE_NEW_MISSION_NAME',
    payload: e.target.value
  }
}

export function addTarget(e: any){
  return {
    type: 'ADD_NEW_MISSION_TARGET',
    payload: e.target.value
  }
}

export function removeTarget(target: any){
  return {
    type: 'REMOVE_NEW_MISSION_TARGET',
    payload: target
  }
}

export function handleConditionChange(e){
  return {
    type: 'UPDATE_NEW_MISSION_CONDITION',
    payload: e.target.value
  }
}

export function handleTypeChange(e){
  return {
    type: 'UPDATE_NEW_MISSION_TYPE',
    payload: e.target.value
  }
}

export function handleLengthChange(e) {
  return {
    type: 'UPDATE_NEW_MISSION_LENGTH',
    payload: e.target.value
  }
}

export function deleteSentence(sentence){
  return {
    type: 'DELETE_NEW_MISSION_SENTENCE',
    payload: sentence
  }
}

export function handleSentenceChange(e){
  return {
    type: 'UPDATE_NEW_MISSION_SENTENCE',
    payload: e.target.value
  }
}

export function saveSentence(){
  return {
    type: 'ADD_NEW_MISSION_SENTENCE'    
  }
}

export function editMission(name) {
  return function (dispatch, getState) {
    let { missions } = getState().missions
    let mission = missions.find(m => m.name == name) 
    dispatch({
      type: 'EDIT_MISSION',
      payload: mission
    })
  }
}