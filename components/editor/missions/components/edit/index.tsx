import * as React from 'react'
import { Route } from 'react-router'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from './actions'
const s = require('./styles.scss')
import { device } from '../../../../../interfaces/devices';

interface root {
  condition: string,
  name: string,
  type: string,
  length: string,
  sentence: string,
  sentences: Array<string>,
  handleConditionChange: Function,
  handleNameChange: Function,
  handleTypeChange: Function,
  handleLengthChange: Function
  handleSentenceChange: Function,
  updateMission: Function,
  deleteMission: Function,
  saveSentence: Function,
  removeTarget: Function,
  deleteSentence: Function,
  addTarget: Function,
  targets: Array<string>
  devices: Array<device>

}

class MissionEditor extends React.Component<root> {
  condition(){
    return (
      <div>
        <label className="input">
          <span>
            Mission condition:
          </span>
          <select 
            value={this.props.condition} 
            onChange={this.props.handleConditionChange.bind(this)}>
            <option value=".*">All have to match a state</option>
            <option value=".">Any have to match a state</option>
            <option value="">An element has to match a state</option>
          </select>
        </label>
      </div>
    )
  }

  render() {
    return (
      <div className="missionsEdit">
        
        <div className="section">
          <label className="input">
            <span>
              Name:
            </span>
            <input 
              value={this.props.name} 
              onChange={this.props.handleNameChange.bind(this)} />
          </label>

          <label className="input">
            <span>
              Type:
            </span>
            <select 
              onChange={this.props.handleTypeChange.bind(this)}
              value={this.props.type}>
              <option value="state">state</option>
              <option value="event">event</option>
            </select>
          </label>

          <label className="input">
            <span>
              Length:
            </span>
            <input 
              onChange={this.props.handleLengthChange.bind(this)}
              type="number" 
              value={this.props.length} />
          </label>
        </div>

        <div className="section">
          Targets devics:
          <div className="tagWrapper">
            {this.props.targets.map(target => {
              return (
                <div key={target}
                className="tagWrapper__tag">
                  {target}
                  <button
                    onClick={this.props.removeTarget.bind(this, target)}>
                    ✖
                  </button>
                </div>
              )
            })}
          </div>

          <label className="input">
            <span>
              All devices
            </span>
            <select value="NOTHING" onChange={this.props.addTarget.bind(this)}>
              <option value="NOTHING">Nothing</option>
              {this.props.devices.map(device => {
                return <option 
                  key={ device.name } 
                  value={ device.name }>
                  { device.name }
                </option>
              })}
            </select>
          </label>
        </div>
        
        <div className="section">
          <Link to={`/editor/missions/${this.props.name}/state`}>State</Link>
          <Link to={`/editor/missions/${this.props.name}/event`}>Event</Link>       
          <Route path="/editor/missions/:name/state" component={this.condition.bind(this)}/>
        </div>

        
        <div className="section">
          <h4>Sentences</h4>

          <div className="sentenceWrapper">
            {this.props.sentences.map((sentence, index) => {
              return <p key={index}>
                <span>{sentence}</span>
                <button 
                  onClick={this.props.deleteSentence.bind(this, sentence)}>
                  ✖
                </button>
              </p>
            })}

            <label className="input">
              <span>
                sentence:
              </span>
              <span className="input__double">
                <textarea 
                  rows={1}
                  onChange={this.props.handleSentenceChange.bind(this)}>
                  {this.props.sentence}
                </textarea>
                <button onClick={this.props.saveSentence.bind(this)}>✔</button>
              </span>
            </label>
            <p>
              `%noun` = name of device<br />
              `%option` = name of capability
            </p>
          </div>
        </div>



        <button onClick={this.props.updateMission.bind(this)}>Save</button>
        <button onClick={this.props.deleteMission.bind(this)}>Delete</button>
      </div>
    )
  }
}

function mapStateToProps(state) {
  console.log(state.mission.targets)
  return {
    devices: state.devices.devices,
    name: state.mission.name,
    targets: state.mission.targets,
    length: state.mission.length,
    condition: state.mission.condition,
    type: state.mission.type,
    sentences: state.mission.sentences,
    sentence: state.mission.sentence
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(MissionEditor)
