import * as React from 'react'
import { connect } from 'react-redux'
import { device } from '../../../../../interfaces/devices'
import { mission } from '../../../../../interfaces/mission'

export function getRandomItem(_array: Array<any>, exclude?:any) {
  let validItems = _array.filter(item => item !== exclude)
  return validItems[Math.floor(Math.random() * validItems.length)]
}

export function getRandomItems(_array: Array<any>, upperBound:number = 3, exclude:Array<any> = []) {
  if (!Array.isArray(exclude)) {
    exclude = [exclude]
  }
  upperBound = Math.ceil(Math.random() * upperBound)
  let items = []
  while (items.length < upperBound) {
    let item = getRandomItem(_array)
    if (exclude.indexOf(item) === -1) {
      items.push(item)
    }
  }
  return items
}

export function createEventTask(device, mission){
  let keys = device.capabilities.map(cap => cap.key)
  return getRandomItems(keys, mission.length, mission.exclude)
}

export function createEventSentence(sentence, task, device){
  let labels = task.map(key => {
    return device.capabilities.find(element => element.key == key).label
  })
  return sentence
    .replace('%noun', 'Jezza')
    .replace('%option', labels.join(', '))
}

interface root {
  devices: Array<device>,
  mission: mission,
}

class Preview extends React.Component<root> {
  render(){
    let validDevices = this.props.devices.filter(dev => {
      return this.props.mission.targets.indexOf(dev.name) > -1      
    })

    return (
      <div>
        {validDevices.map(device => {

          let task = createEventTask(device, this.props.mission)

          return <div key={device.name}>
            {`Dev: ${device.name}`}
            <ul>
              {this.props.mission.sentences.map((sentence, index) => {
                return <li key={index}>
                  {createEventSentence(sentence, task, device)}
                </li>
              })}
            </ul>
          </div>
        })}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    mission: state.mission,
    devices: state.devices.devices,
    sentence: state.mission.sentence
  }
}

export default connect(mapStateToProps)(Preview)
