import * as React from 'react'
import { Route } from 'react-router'

const s = require('./styles.scss')

import List from './components/list'
import MissionEditor from './components/edit'
import Preview from './components/preview'

export default class Missions extends React.Component {
  render() {
    return (
      <div className="missions">
        <div>
          <h2>Missions</h2>
          <List />
        </div>
        <Route path='/editor/missions/:name' component={MissionEditor} />
        <Route path='/editor/missions/:name' component={Preview} />
      </div>
    )
  }
}