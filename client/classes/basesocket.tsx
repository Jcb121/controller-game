import * as observer from 'node-observer'
import * as _constants from '../../data/constants.json'
const constants: any = _constants;

export default class BaseSocket {

  socket: any

  close(){
    console.log('Connection closed')
    observer.unsubscribe(this, constants.connection.send, this.send.bind(this))
    observer.send(this, constants.connection.close)
  }
  error(){
    console.log('Failed to connect to the server')
    observer.unsubscribe(this, constants.connection.send, this.send.bind(this))
    observer.send(this, constants.connection.error)
  }
  open(){
    observer.subscribe(this, constants.connection.send, this.send.bind(this))
    this.socket.onmessage = this.receive.bind(this)
    console.log('Connection established')
    observer.send(this, constants.connection.open)
  }
  send(who, {type, payload}){
    console.log('SENDING!', {type, payload})
    this.socket.send(JSON.stringify(
      {type, payload}
    ))
  }
  receive({data}){
    console.log('Message', data)
    let {type, payload} = JSON.parse(data)
    observer.send('SERVER', type, payload)
  }
}