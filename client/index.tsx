/*
 * This is the game code. is it needed?
 * This will only communicate via the observer
 * nothing else.
 */

import * as observer from 'node-observer'
import * as _constants from '../data/constants.json'
const constants: any = _constants;

console.log('Client Running')

const defaultState = {
  system: {
    socket: false
  },
  player: {
    name: false,
    modules: false,
  }
}

// This need to enforce a protocol.

export default class Game {
  state: any
  constructor(){
    this.state = defaultState
    observer.subscribe(this, constants.player.rename, this.renamePlayer.bind(this))    
    observer.subscribe(this, constants.connection.open, function () {
      console.log('CLIENT_READY')
      observer.send(this, constants.client.ready)
    })
  }
  
  renamePlayer(who, {name}){
    console.log(`Player's name is now ${name}`)
    this.state.player.name = name 
  }

  send(node, message){
    observer.send(this, constants.connection.send, message)
  }
}