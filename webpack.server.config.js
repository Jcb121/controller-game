var fs = require('fs')
var path = require('path')
const WebpackShellPlugin = require('webpack-shell-plugin')

module.exports = {

  entry: path.resolve(__dirname, 'server/index.tsx'),
  devtool: 'inline-source-map',
  output: {
    filename: './dist/server.bundle.js'
  },

  target: 'node',

  plugins: [
    new WebpackShellPlugin({ onBuildStart: ['echo "Webpack Start"'], onBuildEnd: ['npm run server:start'] })
  ],

  // keep node_module paths out of the bundle
  externals: fs.readdirSync(path.resolve(__dirname, 'node_modules')).concat([
    'react-dom/server', 'react/addons',
  ]).reduce(function (ext, mod) {
    ext[mod] = 'commonjs ' + mod
    return ext
  }, {}),

  node: {
    __filename: true,
    __dirname: true
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    alias: {
      "constants.json": path.resolve(__dirname, 'data/constants.json'),
      stateMissions: path.resolve(__dirname, 'data/state.missions.json'),
      eventMissions: path.resolve(__dirname, 'data/event.missions.json'),
      nouns: path.resolve(__dirname, 'data/nouns.json'),
      captains: path.resolve(__dirname, 'data/captains.json'),


    }
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: [ /node_modules/ ],
        use: 'awesome-typescript-loader'
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        use: 'source-map-loader'
      },
      {
        test: /\.scss$/,
        loader: [
          'css-loader/locals?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]',
          'sass-loader'
        ]
      }
      // {
      //   test: /\.js$/,
      //   exclude: /node_modules/,
      //   loader: 'babel-loader?presets[]=es2015&presets[]=react'
      // }
    ]
  }
}