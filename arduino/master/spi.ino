extern const int MODULES_LENGTH;
extern const int MODULE_PINS[MODULES_LENGTH];
const String START_TYPE = "START";
// SPI COMMANDS
const byte GET_TYPE = 'T';
const byte GET_BUFFER = 'B';
const byte GET_STATE = 'A';
const byte SET_COMMAND = 'C';
const byte SET_SCREEN = 'S';
const byte SET_LED = 'L';
const byte GET_IO = 'I';

void spiSetup() {
  // SETUP SPI select pins
  for (int i = 0; i < MODULES_LENGTH; i++) {
    pinMode(MODULE_PINS[i], OUTPUT);
    digitalWrite(MODULE_PINS[i], HIGH);
  }
  SPI.begin ();
}

byte transferAndWait (const byte what) {
  byte a = SPI.transfer (what);
  delayMicroseconds (15);
  return a;
}

byte getModuleIOLength(int pin) {
  if (pin == INBUILT_MODULE) {
    return 1;
  } else {
    byte ioLength;
    digitalWrite(pin, LOW);
    delay(15);
    transferAndWait(GET_IO);
    transferAndWait('0');
    ioLength = transferAndWait('0');
    delay(15);
    digitalWrite(pin, HIGH);
    return ioLength;
  }
}

void getModuleState(int pin, int numberOfPins) {
  if (pin == INBUILT_MODULE) {
    getStartState();
  } else {
    digitalWrite(pin, LOW);
    delay(15);
    transferAndWait(GET_STATE);
    transferAndWait('0'); // return null
    for (int i = 0; i < numberOfPins; i++) {
      stateBuffer[i] = transferAndWait((i + 1)); // 0 already sent + 1;
    }
    delay(15);
    digitalWrite(pin, HIGH);
  }
}

void getModuleBuffer(int pin) {
  if (pin == INBUILT_MODULE) {
    getStartBuffer();
  } else {
    digitalWrite(pin, LOW);
    delay(15);
    transferAndWait(GET_BUFFER);
    transferAndWait('0'); // return null
    for (int i = 0; i < BUFFER_SIZE; i++) {
      eventBuffer[i] = transferAndWait((i + 1)); // 0 already sent + 1;
    }
    delay(15);
    digitalWrite(pin, HIGH);
  }
}

String getModuleType(int pin) {
  if (pin == INBUILT_MODULE) {
    return START_TYPE; // return
  } else {
    String moduleType;
    digitalWrite(pin, LOW);
    delay(15);
    transferAndWait(GET_TYPE);
    transferAndWait('0'); // returns NULL
    for (int i = 0; i < BUFFER_SIZE; i++) {
      moduleType += (char) transferAndWait((i + 1)); // 0 already sent + 1
    }
    delay(15);
    digitalWrite(pin, HIGH);

    moduleType.trim();
    return moduleType;
  }
}


