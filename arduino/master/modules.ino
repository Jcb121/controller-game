boolean modulesSetup = false; // setup with Master
boolean modulesReady = false; // setup with server

// check if the module is setup,
// Ready for a serial connection.
boolean areModulesSetup() {
  if (modulesSetup == true) {
    return true;
  } else {
    for (int i = 0; i < MODULES_LENGTH; i++) {
      if (MODULE_TYPES[i] == "") {
        return false;
      }
    }
    modulesSetup = true;
    return true;
  }
}

// check if the module is ready
// Ready for games communication
boolean areModulesReady() {
  if (modulesReady == true) {
    return true;
  } else {
    for (int i = 0; i < MODULES_LENGTH; i++) {
      if (MODULE_IDS[i] == "") {
        return false;
      }
    }
    modulesReady = true;
    return true;
  }
}


// get the type of each module
void getModuleTypes() {  
  if (areModulesSetup() == false) {
    for (int i = 0; i < MODULES_LENGTH; i++) {
      MODULE_TYPES[i] = getModuleType(MODULE_PINS[i]);
      MODULE_IO_LENGTH[i] = getModuleIOLength(MODULE_PINS[i]);
    }
  }
}


