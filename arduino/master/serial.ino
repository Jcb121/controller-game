const char DELIMITER = '|';
const int SERIAL_DEBOUNCE_DELAY = 2000;
// combo
int serialPosition = 0;
String serialMessage[3] = {"", "", ""};
boolean serialReady = false;
boolean serialStringComplete = false;
int serialDebounce = SERIAL_DEBOUNCE_DELAY;
void clearSerialMessage() {
  serialMessage[0] = "";
  serialMessage[1] = "";
  serialMessage[2] = "";
}

void serialSetup() {
  Serial.begin(115200);
}

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char) Serial.read();
    if (inChar == '\n') {
      serialStringComplete = true;
    } else if (inChar == DELIMITER) {
      serialPosition++;
    } else {
      serialMessage[serialPosition] += inChar;
    }
  }
}

boolean debounceSerial() {
  if (serialDebounce >= SERIAL_DEBOUNCE_DELAY) {
    serialDebounce = 0;
    return true;
  } else {
    serialDebounce += deltaTime;
    return false;
  }
}



boolean isSerialStringComplete() {
  if (serialStringComplete) {
    serialStringComplete = false;
    serialPosition = 0;
    return true;
  } else {
    return false;
  }
}

void sendReady() {
  if (serialReady == false) {
    serialReady = true;
    Serial.println("00000000 SERIAL_READY");
  }
}

boolean isSerialReady() {
  if (serialReady) {
    return true;
  } else {
    if (!Serial.available()) {
      return true;
    } else {
      return false;
    }
  }
}

boolean isBufferEmpty() {
  for (int i = 0; i < BUFFER_SIZE; i++) {
    if (eventBuffer[i] != ' ') {
      return false;
    }
  }
  return true;
}

boolean sendBuffer(int index) {
  Serial.print(MODULE_IDS[index]);
  Serial.print(DELIMITER);
  
  Serial.print("EVENT_BUFFER");
  Serial.print(DELIMITER);
  
  for (int i = 0; i < BUFFER_SIZE; i++) {
    Serial.print((char)eventBuffer[i]);
  }
  Serial.println();
}

boolean sendState(int index) {
  Serial.print(MODULE_IDS[index]);
  Serial.print(DELIMITER);
  Serial.print("STATE_BUFFER");
  Serial.print(DELIMITER);
  for (int i = 0; i < MODULE_IO_LENGTH[index]; i++) {
    Serial.print(stateBuffer[i], DEC);
  }
  Serial.println();
}

void emptyBuffer() {
  for (int i = 0; i < BUFFER_SIZE; i++) {
    eventBuffer[i] = ' ';
  }
}

void emptyState() {
  for (int i = 0; i < BUFFER_SIZE; i++) {
    stateBuffer[i] = ' ';
  }
}

void serialUpdate() {
  // MODULES ARE SETUP
  if (isSerialReady()) {
    // ready for comms
    sendReady(); // fires ready signal
    if (areModulesReady()) {
      // check if the modules are readdy for the game
      if (debounceSerial()) {
        for (int i = 0; i < MODULES_LENGTH; i++) {
          int modulePin = MODULE_PINS[i];
          // emptyBuffer();
          getModuleBuffer(modulePin);
          if (isBufferEmpty() == false) {
            sendBuffer(i);
            delay(5);
            // emptyState(); // shouldn't be needed
            getModuleState(modulePin, MODULE_IO_LENGTH[i]);
            sendState(i);
          }  
        }
      }
    }
    // handles responding to serial
    if (isSerialStringComplete()) {
      handleSerialMessage(serialMessage[0], serialMessage[1], serialMessage[2]);
      clearSerialMessage();
    }
  }
}


