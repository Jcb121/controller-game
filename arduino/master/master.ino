#include <SPI.h>
unsigned long deltaTime;
unsigned long then;

const boolean DEV = false;

const byte BUFFER_SIZE = 8;
const int INBUILT_MODULE = -1;

byte eventBuffer[8] = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};
byte stateBuffer[8] = { 0, 0, 0, 0, 0, 0, 0, 0};

// set by master
const int MODULES_LENGTH = 3;
// set by modules
String MODULE_TYPES[MODULES_LENGTH] = {"", "", ""};
byte MODULE_IO_LENGTH[MODULES_LENGTH] = {0, 0, 0};

// set by server
String MODULE_IDS[MODULES_LENGTH] = {"", "", ""};
// set by board
const int MODULE_PINS[MODULES_LENGTH] = {INBUILT_MODULE, SS, 9};


void setup() {

  if (DEV) {
    MODULE_IDS[0] = "AAAAAAAA";
    MODULE_IDS[1] = "BBBBBBBB";
    MODULE_IDS[2] = "CCCCCCCC";
  }

  spiSetup();
  serialSetup();
  startModuleSetup();
  then = millis();
}

void loop() {
  deltaTime = millis() - then;

  // spi is setup first
  if (areModulesSetup()) {
    startModuleUpdate();
    serialUpdate();
  } else {
    getModuleTypes();
  }

  then += deltaTime;
}


