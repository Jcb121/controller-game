extern const char DELIMITER;

void handleSerialMessage(String messageId, String command, String argument) {
  Serial.println("HERE:" + command);
  Serial.print(messageId + DELIMITER);
  if (command == "IS_GAME_MASTER") {
    isGameMaster();
  } else if (command == "LIST_MODULES") {
    listModules();
  } else if (command == "SET_MODULE_IDS") {
    assignIds(argument);
  } else {
    Serial.println("COMMAND " + command + " NOT SUPPORTED ARG " + argument);
  }
  Serial.println();
}

// print true to being a game master
void isGameMaster() {
  Serial.print("TRUE");
}

// Prints the module types on the serial bus
void listModules() {
  if (areModulesSetup()) {
    for (int i = 0; i < MODULES_LENGTH; i++) {
      Serial.print(MODULE_TYPES[i]);
      if (i < MODULES_LENGTH - 1) {
        Serial.print(',');
      }
    }
  }
}

void assignIds(String line) {
  int linePosition = 0;
  for (int i = 0; i < line.length(); i++) {
    if (line[i] == ',') {
      linePosition++;
    } else {
      MODULE_IDS[linePosition] += line[i];
    }
  }
  Serial.print("TRUE");
}

