extern const byte BUFFER_SIZE;
const int START_PIN = 3;
const int BUTTON_DEBOUNCE_DELAY = 50;
const char BUTTON_NAME = 'S';
boolean startState;
int startDebounce = BUTTON_DEBOUNCE_DELAY;
int startIndex = 0;
byte startBuffer[BUFFER_SIZE] = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '};
boolean startBufferFull = false;

boolean debounceStart() {
  if (startDebounce >= BUTTON_DEBOUNCE_DELAY) {
    startDebounce = 0;
    return true;
  } else {
    startDebounce += deltaTime;
    return false;
  }
}

void startModuleSetup() {
  pinMode(START_PIN, INPUT);
}

void startModuleUpdate() {
  if (debounceStart()) {
    readStartPin();
  }
  // if (isStartBufferFull()) {
  //   sendBuffer(startIndex);
  // }
}

boolean isStartBufferFull() {
  if (startBufferFull) {
    startBufferFull = false;
    return true;
  } else {
    return false;
  }
}

void readStartPin() {
  boolean state =  digitalRead(START_PIN);
  if (startState != state) {
    startState = state;
    if (state == LOW) { // Down!
      srartButtonPress();
    }
  }
}

void srartButtonPress() {
  for (int i = 0; i < BUFFER_SIZE; i++) {
    // if (i == BUFFER_SIZE - 1) {
    //   startBufferFull = true; // only care about start button
    // }
    if (startBuffer[i] == ' ') {
      startBuffer[i] = BUTTON_NAME;
      break;
    }
  }
}

void getStartBuffer() {
  for (int i = 0; i < BUFFER_SIZE; i++) {
    eventBuffer[i] = startBuffer[i];
    startBuffer[i] = ' '; // clear startBuffer
  }
}

boolean getStartState() {
  stateBuffer[0] = startState;
}

