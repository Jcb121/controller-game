byte inputStates[NUMBER_OF_PINS]; // Use state for monitoring debounce. // 1 = default/
boolean ledStates[NUMBER_OF_PINS];
int debounceMap[NUMBER_OF_PINS];

boolean inputChanged(byte newState, int index) {
  if (inputStates[index] != newState) {
    // state has changed
    inputStates[index] = newState; // set the new state

    if (TYPE == "BUTTONS" && newState == HIGH) {
      // if it's a button on the return stroke
      return false;
    } else {
      // if state has changed return true!
      return true;
    }
  } else {
    return false;
  }
}

void controlsSetup() {
  for (int i = 0; i < NUMBER_OF_PINS; i++) {
    ledStates[i] = LOW; // turn off leds
    debounceMap[i] = debounceNew(); // get a debounce ID


    if (TYPE == "BUTTONS") {
      pinMode(INPUT_PINS[i], INPUT);
      pinMode(OUTPUT_PINS[i], OUTPUT);
      inputStates[i] = HIGH; // default to high/off
    } else if ( TYPE == "DIALS") {
      // no setup needed on ANALOG PINS?
      pinMode(OUTPUT_PINS[i], OUTPUT);
      inputStates[i] = 0;
    }
  }
}

void controlsUpdate() {
  for (int i = 0; i < NUMBER_OF_PINS; i++) {
    int debounceId = debounceMap[i];
    if (debounceUpdate(debounceId)) {
      if (TYPE == "BUTTONS") {
        byte newState = digitalRead(INPUT_PINS[i]); // return 0/1
        boolean changed = inputChanged(newState, i); // compare last state
        if (changed) {
          addToBuffer(BUTTON_NAMES[i]);
        }
      } else if (TYPE == "DIALS") {
        int state = analogRead(INPUT_PINS[i]);
        byte newState = map(state, 0, 1023, 0, 255);
        boolean changed = inputChanged(newState, i);
        if (changed) {
          addToBuffer(BUTTON_NAMES[i]);
        }
      }
    }
  }
}
