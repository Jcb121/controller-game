String moduleName = "";
String gameTitle = "Controller Game ";
int debounceId;

void lcdSetup() {
  moduleName.reserve(16);
  lcd.init();
  lcd.backlight();

  lcd.setCursor(0, 0);
  lcd.print(gameTitle);
  lcd.setCursor(2, 1);
  lcd.print("Press Start!");
  debounceId = debounceNew(); // get a debounce ID
}

void setText(String text) {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(text);
}

void lcdUpdate() {
  if (debounceUpdate(debounceId)) {
    // do something cool in here
  }
}


