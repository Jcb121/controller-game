const int NUMBER_OF_PINS = 3;
const int INPUT_PINS[NUMBER_OF_PINS] = {A0, A1, A2};
const int OUTPUT_PINS[NUMBER_OF_PINS] = {9, 8, 7};
const byte BUTTON_NAMES[NUMBER_OF_PINS] = {'A', 'B', 'C'};
const byte MODULE_TYPE[BUFFER_SIZE] = {' ', ' ', ' ', 'D', 'I', 'A', 'L', 'S'}; // must be 8 chars
const String TYPE = "DIALS";
