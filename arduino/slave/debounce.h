int debounce[8] = { -1, -1, -1, -1, -1, -1, -1, -1};
// the debounce time; increase if the output flickers
const int DEBOUNCE_DELAY[8] = {50, 50, 50, 50, 50, 50, 1000, 0};

const int DEBOUNCE_LENGTH = 8;

// Loop
unsigned long deltaTime;
unsigned long then;

int debounceNew() {
  int debounceIndex = 0;
  while (true) {
    if (debounce[debounceIndex] == -1) {
      break;
    }
    debounceIndex++;
  }
  debounce[debounceIndex] = DEBOUNCE_DELAY[debounceIndex];
  return debounceIndex;
}

void debounceSetup() {
  then = millis();
}

void debounceStart() {
  deltaTime = millis() - then;
}

void debounceEnd() {
  then += deltaTime;
}

boolean debounceUpdate(int debounceIndex) {
  if (debounce[debounceIndex] >= DEBOUNCE_DELAY[debounceIndex]) {
    debounce[debounceIndex] = 0;
    return true;
  } else {
    debounce[debounceIndex] += deltaTime;
    return false;
  }
}

