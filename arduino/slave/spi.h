// SPI COMMANDS
const byte GET_TYPE = 'T';
const byte GET_BUFFER = 'B';
const byte GET_STATE = 'A';
const byte SET_COMMAND = 'C';
const byte SET_SCREEN = 'S';
const byte SET_LED = 'L';
const byte GET_IO = 'I';

volatile byte command = SET_COMMAND;
int spiPosition = -1;
extern String moduleName;
extern const int NUMBER_OF_PINS;
extern boolean ledStates[NUMBER_OF_PINS];
extern byte inputStates[NUMBER_OF_PINS];

void ss_rising () {
  // When device is deactivating!
  spiPosition = -1;
  switch (command) {
    case GET_STATE:
      break;
    case GET_BUFFER:
      emptyBuffer();
      break;
    case SET_SCREEN:
      setText(moduleName);
      break;
    case SET_LED:
      break;
    default:
      break;
  }
  command = SET_COMMAND;
}

void spiSetup() {
  // have to send on master in, *slave out*
  pinMode(MISO, OUTPUT);

  // turn on SPI in slave mode
  SPCR |= _BV(SPE);

  // turn on interrupts
  SPCR |= _BV(SPIE);

  // interrupt for SS rising edge
  attachInterrupt (0, ss_rising, RISING);
}

// SPI interrupt routine
ISR (SPI_STC_vect) {
  byte c = SPDR; // check the buffer for 0!
  spiPosition++;
  switch (command) {
    case GET_IO:
      SPDR = NUMBER_OF_PINS;
      break;
    case GET_STATE:
      if (spiPosition < NUMBER_OF_PINS) { // 0 < 3, 1 < 3, 2 < 3
        SPDR = inputStates[spiPosition];
      } else {
        SPDR = ' '; // Send back null;
      }
      break;
    case GET_BUFFER:
      SPDR = getBuffer(spiPosition); // send the current buffer
      break;
    case SET_COMMAND:
      command = c;
      SPDR = 'Z';
      spiPosition = -1;
      break;
    case GET_TYPE:
      SPDR = MODULE_TYPE[spiPosition]; // send the current buffer
      break;
    case SET_SCREEN:
      // add the byte onto the name, LCD command == slow
      moduleName += c;
      break;
    case SET_LED:
      ledStates[(int) c] = 1;
      break;
    default:
      SPDR = 'X';
      break;
  }
}
