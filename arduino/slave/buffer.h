byte eventBuffer[BUFFER_SIZE] = {'Z', 'Y', 'X', 'W', 'V', 'U', 'T', 'S'}; // 8 x bytes

void addToBuffer(byte data) {
  if (data == ' ') {
    return; // return null data
  }
  for (int i = 0; i < BUFFER_SIZE; i++) {
    if (eventBuffer[i] == ' ') {
      eventBuffer[i] = data;
      break;
    }
  }
}

void emptyBuffer() {
  for (int i = 0; i < BUFFER_SIZE; i++) {
    eventBuffer[i] = ' '; // This is filling state buffer!!
  }
}

byte getBuffer(int index) {
  return eventBuffer[index];
}
