
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display

// SPI BUFFER
const int BUFFER_SIZE = 8;

#include "buffer.h"
#include "debounce.h"
#include "lcd.h"
// #include "dials.h"
#include "buttons.h"
#include "spi.h"

extern const int NUMBER_OF_PINS;
extern const int OUTPUT_PINS[NUMBER_OF_PINS];

void setLeds() {
  for (int i = 0; i < NUMBER_OF_PINS; i++) {
    digitalWrite(OUTPUT_PINS[i], ledStates[i]);
  }
}

void setup() {
  debounceSetup();
  controlsSetup(); // takes up first six debounces
  lcdSetup(); // takes up seventh debounce
  spiSetup();
}

void loop() {
  debounceStart();
  /* Code here */
  controlsUpdate();
  setLeds();
  lcdUpdate();
  /* Code over */
  debounceEnd();
}



