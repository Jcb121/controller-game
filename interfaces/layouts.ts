import { module } from "../interfaces/modules";

export interface rootLayout {
  gridTemplateRows: string,
  gridTemplateColumns : string
}

export interface layout {
  layoutName?: string,
  layout: rootLayout
  modules: Array<module>
}