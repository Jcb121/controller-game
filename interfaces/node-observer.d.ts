
declare module 'node-observer' {
  export let subscribe: Function;
  export let send: Function;
  export let unsubscribe: Function;
}