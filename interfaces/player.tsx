import { setupModule } from "./modules";
import { layout } from "./layouts";

export interface player extends layout{
  id?: string,
  modules: Array<setupModule>
}

export const defaultPlayer: player = {
  modules: [],
  layout: {
    gridTemplateRows: 'auto',
    gridTemplateColumns: 'auto'
  }
}