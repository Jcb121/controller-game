export interface capability {
  key: string,
  label: string
}
export interface option {
  key: string,
  label: string
}
export interface device {
 name: string,
 emulator: string,
 isNameable: boolean,
 options: Array<option>,
 capabilities: Array<capability>,
 prevOptions?: Array<option>
}
