import { capability, option } from "./devices";

export interface location {
  gridColumnStart?: number,
  gridColumnEnd?: number,
  gridRowStart?: number,
  gridRowEnd?: number,
  gridTemplateRows?: string,
  gridTemplateColumns?: string
}

export interface module {
  capabilities: Array<capability>,
  element: string,
  layout?: object,
  location: location,
  name?: string,
  options: Array<option>,
  type: string
}

export interface setupModule extends module{
  id: string,
  state: object,
}