export interface mission {
  name: string,
  targets: Array<string>,
  length: string,
  condition: string,
  type: 'event' | 'state',
  sentences: Array<string>,
  sentence: string
}

export interface renderedMission {
  sentence: string,
  targetTime: number
}

export interface renderedCaptain {
  
}