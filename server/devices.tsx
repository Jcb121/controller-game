import * as fs from 'fs'
import { device } from '../interfaces/devices';

export async function updateDevice(req, res){
  let devices = await readFile()
  let index = devices.findIndex(d => d.name == req.body.name)
  if (index > -1) {
    devices[index] = req.body
  } else {
    devices.push(req.body)
  }
  await writeFile(devices)
  res.send('{"payload": "Device Saved"}')
}

export async function deleteDevice(req, res){
  let devices = await readFile()
  devices = devices.filter(device => device.name != req.params.deviceName)
  writeFile(devices)
  res.send('{"payload": "Design Deleted"}')
}

export async function getDevices(req, res){
  let devices = await readFile()
  let device = devices.map(dev => dev.name)
  res(JSON.stringify(device))
}

function readFile(){
  return new Promise<Array<device>>((resolve: Function, reject: Function) => {
    fs.readFile('./data/devices.json', 'utf8', (err, data) => {
      if(err){
        reject(err)
        return
      } 
      resolve(JSON.parse(data))
      return
    })
  })
}

function writeFile(data){
  return new Promise((resolve: Function, reject: Function) => {
    fs.writeFile('./data/devices.json', JSON.stringify(data), (err) => {
      if (err) {
        reject(err)
        return
      } 
      resolve()
      return
    })
  })
}