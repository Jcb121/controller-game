const PORT = process.env.NODE_ENV === 'production' ? 80 : 8000
var ws = require('nodejs-websocket')
var observer = require('node-observer')
var path = require('path')

import * as _constants from '../data/constants.json'
const constants: any = _constants;
import Game from './game'
import * as fs from 'fs'
import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as React from 'react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import { renderToString } from 'react-dom/server'
import Player from './player'

import { updateDevice, deleteDevice, getDevices } from './devices'
import { deleteLayout, updateLayout, layoutNames, getAnyLayout, getLayout } from './layouts'
import { updateMission, deleteMission } from './missions'

const LAYOUTS = require('../data/layouts.json')
const missions = require( '../data/missions.json')
const devices = require( '../data/devices.json')


console.log('server started')

const app = express()
const dataFolder = './data/layouts/'

var rooms = []
const players = {}

setInterval(function () {
  console.log('index - cleaningRooms', new Date().toISOString(), 'Current Rooms', rooms.length)
  let toRemove = []
  rooms.forEach((room, index) => {
    if (room.game.players.length < 1) {
      console.log('index - removing room', room.name)
      toRemove.push(index)
    }
  })
  toRemove.forEach(index => rooms.splice(index, 1))
}, 15000)

ws.createServer(function (conn) {
  const regex = /.*?rooms\/(.*?)\/(.*?)$/;
  let [path, targetRoom, playerId] = regex.exec(conn.path)
  let room = rooms.find(room => room.name === targetRoom)
  if (!room){
    console.log('Creating new room', targetRoom)
    room = {
      name: targetRoom,
      game: new Game()
    }
    rooms.push(room)
  }
  let player = players[playerId]
  delete players[playerId]
  player.addConnection(conn)
  console.log(`Player ${player.id} joined room ${room.name}`)
  observer.send(room, constants.player.joined, player)
}).listen(8001)

app.use(bodyParser())
app.use(express.static('./dist'))


// get the game finder // Server side rendering!
import gameFinderTemplate from '../targets/gamefinder/template'
import GameFinder from '../components/finder/index'
app.get('/', (req, res) => {
  const initialState = {
    rooms: rooms.map( room => {
      return {
        name: room.name,
        players: room.game.players.length
      }
    })
  }
  const appString = renderToString(<GameFinder {...initialState}/>)
  res.send(gameFinderTemplate({
    body: appString,
    title: 'Game Finder',
    initialState: JSON.stringify(initialState)
  }))
})

// get the game and join certain room
import webTemplate from '../targets/web/template'
import Controller from '../components/webcontroller/index'
app.get('/rooms/:roomName', (req, res) => {
  let layout = LAYOUTS[Math.floor(Math.random() * LAYOUTS.length)]
  const player = new Player(layout)
  players[player.id] = player
  const initialState = { player }
  const appString = renderToString(<Controller player={player.render()} />)
  res.send(webTemplate({
    body: appString,
    title: `Room - ${req.params.roomName}`,
    initialState: JSON.stringify(initialState)
  }))
})

// get the editor
import { StaticRouter } from 'react-router'
import editorTemplate from '../targets/editor/template.js'
import Editor from '../components/editor/index'
import EditorApp from '../components/editor/reducers/index'
import { player } from '../interfaces/player';

app.get(['/editor', '/editor/*'], (req, res) => {
  
  const defaultState = {
    devices: { devices },
    missions: { missions },
    mission: null
  }
  
  const missionRegex = /missions\/(.*?)$/g;
  let result = missionRegex.exec(req.params[0])
  if(result){
    var missionName = result[1]
    const mission = missions.find(m => m.name == missionName)
    defaultState.mission = mission
  }
  
  const store = createStore(EditorApp, defaultState)

  const appString = renderToString(
    <Provider store={store}>
      <StaticRouter location={req.url}>
        <Editor />
      </StaticRouter>
    </Provider>
  )
  const initialState = store.getState()
  res.send(editorTemplate({
    body: appString,
    title: `Room - ${req.params.roomName}`,
    initialState: JSON.stringify(initialState)
  }))
})

// Delete the layout
app.delete('/layouts/:layoutName', deleteLayout)

// Delete the device
app.delete('/devices/:deviceName', deleteDevice )

// Delete the Mission
app.delete('/missions/:missionName', deleteMission )

// Save the Layout
app.post('/layouts', updateLayout )

// Save the mission
app.post('/missions', updateMission)

// save the Device
app.post('/devices', updateDevice )

// get list of layouts
app.get('/layouts', layoutNames )

// get list of devices
app.get('/devices', getDevices )

//get specific layout
app.get('/layouts/:layoutName', getLayout )

//get any layout
app.get('/layouts', getAnyLayout )

app.listen(PORT, function () {
  console.log(`Controller Game listening on port ${PORT}!`)
})
