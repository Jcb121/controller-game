import EventOrder from './order/event'
import StateOrder from './order/state'
const nouns = require('nouns')
import * as _constants from '../data/constants.json'
const constants: any = _constants;
const eventMissions = require('eventMissions')
const stateMissions = require('stateMissions')
var observer = require('node-observer')

import Game from './game'
import Player from './player'
import { renderedMission } from '../interfaces/mission'

// TODO: Captains have no refernce to their orders. 
// Orders are the parent of Player and Player is the child of captain

interface identity {
  name: string,
  orderTime: number,
  roundTime: number,
  roundFails: number
}

function getRandomItem(_array: Array<any>, exclude?: any): any {
  let validItems = _array.filter(item => item !== exclude)
  return validItems[Math.floor(Math.random() * validItems.length)]
}


export default class Captain {

  name: string
  game: Game
  failed: Array<renderedMission>
  completed: Array<renderedMission>
  identity: identity
  complete: boolean
  players: Array<Player>

  targetTime: number
  timeOut: any

  constructor(game: Game, identity: identity, players: Array<Player>) {
    console.log('Captain - New Captain created', identity, players)
    this.name = identity.name
    this.game = game
    this.identity = identity
    // the captain should not care about players. only modules

    this.complete = false
    this.failed = [] //Keep track of failed missions
    this.completed = [] //keep track of passed missions
    this.players = [...players] 
    this.nameModules()

    this.players.forEach(player => {
      // player is the Reader, not he do-er
      let order = this.createOrder(player)
      player.updateStage({order: order.render()})
    })

    this.targetTime = Date.now() + identity.roundTime * 1000
    this.timeOut = setTimeout(function () {
      console.log('Captain - Round time out fired')
      this.checkComplete()
    }.bind(this), identity.roundTime * 1000)

    observer.subscribe(this, constants.player.wrongModule, this.handleFuckup.bind(this))
    observer.subscribe(this, constants.player.wrongElement, this.handleFuckup.bind(this))
    observer.subscribe(this, constants.player.correct, this.compliment.bind(this))
    observer.subscribe(this, constants.order.completed, this.completeOrder.bind(this))
    observer.subscribe(this, constants.order.failed, this.failOrder.bind(this))
  }

  destructor() {
    observer.unsubscribe(this, constants.player.wrongModule)
    observer.unsubscribe(this, constants.player.wrongElement)
    observer.unsubscribe(this, constants.player.correct)
    observer.unsubscribe(this, constants.order.completed)
    observer.unsubscribe(this, constants.order.failed)
    clearTimeout(this.timeOut)
  }

  compliment(order, player) {
    player.score++
  }

  handleFuckup(order, player) {
    // not valid
  }

  completeOrder(order) {
    // does this order belong to this captain
    if (order.captain !== this) return

    console.log('Complete Order', order)
    if (order.playerA !== order.playerB) {
      //score only for team work
      order.playerA.score++
      order.playerB.score++
    }
    this.completed.push(order.render())
    // get the reader from the last order

    observer.send(this.game, constants.game.broadcast, {
      type: constants.order.completed
    })

    if (this.checkComplete() === false) {
      // give a new order only if the round has eneded  
      let newOrder = this.createOrder(order.playerA)
      order.playerA.updateStage({order: newOrder.render()})
    }
  }

  failOrder(order) {
    // does this order belong to this captain
    if(order.captain !== this) return

    console.log('Captain - order failed', order)
    this.failed.push(order.render())

    if (this.checkComplete() === false) {
      console.log('Captain - round isn\'t over')
      if (order.playerA !== order.playerB) {
        order.playerA.score--
        order.playerB.score--
      } else {
        order.playerA.score -= 5
      }

      // send the failed mission to all players
      observer.send(this.game, constants.game.broadcast, {
        type: constants.game.updateStage,
        payload: {
          orderFails: this.failed.length          
        }
      })

      // send the correct player the new order
      let newOrder = this.createOrder(order.playerA)
      order.playerA.updateStage({ order: newOrder.render() })
    }
  }

  createOrder(playerA) {
    // reader, do-er, module...
    // the order shouldn't pick the module, but a listener is needed.
    let playerB = getRandomItem(this.players)
    let safeModules = playerB.modules.filter(module => module.type !== 'START')
    let { id, type } = getRandomItem(safeModules)
    let { mission, type: missionType } = this.findMission(type)

    if (missionType === 'state') {
      return new StateOrder(this, playerA, playerB, id, mission, this.identity.orderTime)
    } else {
      return new EventOrder(this, playerA, playerB, id, mission, this.identity.orderTime)
    }
  }

  findMission(type) {
    console.log(`Finding orders for type: ${type}`)
    let missions = { event: eventMissions, state: stateMissions }
    let mType = 'event'

    // create an object of relevant missions
    let missionTypes = Object.keys(missions).reduce((object, mType) => {
      object[mType] = Object.keys(missions[mType]).filter(_missionsTypes => {
        return missions[mType][_missionsTypes].targets.indexOf(type) > -1
      })
      return object
    }, {})

    // randomly decide if it will be state/event
    // 0 * (3 + 3) = 0 >= 3 === FALSE
    // .5 * (3 + 3) = 3 >= 3 === TRUE
    // 1 * (3 + 3) = 6 >= 3 === TRUE
    let direction = Math.floor(Math.random() * (missionTypes['event'].length + missionTypes['state'].length))
    if (direction >= missionTypes['event'].length) {
      mType = 'state'
    }

    // concat all the missions into an array
    var orders = missionTypes[mType].reduce((a, _missionsTypes) => {
      return a.concat(missions[mType][_missionsTypes])
    }, [])

    if (orders.length === 0) {
      throw new Error(`No missions defined for device ${type}`)
    }

    // return a single item
    return {
      mission: getRandomItem(orders),
      type: mType
    }
  }

  addPlayer(player) {
    console.log('Adding new player to round')
    this.players.push(player)
    this.namePlayerModules(player)
    let order = this.createOrder(player)
    player.changeStage('Space', 'Missions')
    player.updateStage({order: order.render()})
  }

  removePlayer(player: Player) {
    console.log('Removing Player from Round', player)
    if (this.players.length === 1) {
      this.players = []
      this.checkComplete()
    }
    else {
      this.players = this.players.filter(_player => _player.id !== player.id)
      // Check for null missions...
      // people can fail the missions
    }
  }

  namePlayerModules(player: Player) {
    let usedNames = this.players.reduce((names, player) => {
      return [
        ...names,
        ...player.modules.reduce((_names, _module) => {
          if (_module.hasOwnProperty('name') && _module.name) {
            _names.push(_module.name)
          }
          return _names
        }, [])
      ]
    }, [])

    let nameAbleModules = player.modules.reduce((_total, _module) => {
      if (_module.hasOwnProperty('name')) {
        _total += 1
      }
      return _total
    }, 0)

    if (usedNames.length + nameAbleModules > nouns.random.length) {
      throw new Error('Not enough Nouns in Random!')
    }

    let newNames = []
    while (newNames.length < nameAbleModules) {
      let newId = Math.floor(Math.random() * nouns.random.length)
      let newName = nouns.random[newId]
      if (usedNames.indexOf(newName) === -1) {
        newNames.push(newName)
      }
    }

    player.modules.forEach(_module => {
      if (_module.hasOwnProperty('name')) {
        _module.name = newNames.shift()
        player.send({
          type: constants.module.rename,
          payload: {
            id: _module.id,
            name: _module.name,
          }
        })
      }
    })
  }

  nameModules() {
    let totalModules = this.players.reduce((total, player) => {
      return total += player.modules.reduce((_total, _module) => {
        if (_module.hasOwnProperty('name')) {
          _total += 1
        }
        return _total
      }, 0)
    }, 0)

    let ids = []
    if (totalModules > nouns.random.length) {
      throw new Error('Not enough Nouns in Random!')
    }
    while (ids.length < totalModules) {
      let newId = Math.floor(Math.random() * nouns.random.length)
      if (ids.indexOf(newId) === -1) {
        ids.push(newId)
      }
    }

    // TODO: use nouns from other categorys based on current module
    this.players.forEach(player => {
      player.modules.forEach(_module => {
        if (_module.hasOwnProperty('name')) {
          let id = ids.shift()
          _module.name = nouns.random[id]
          // Meta!
          player.send({
            type: constants.module.rename,
            payload: {
              id: _module.id,
              name: _module.name,
            }
          })
        }
      })
    })
  }

  render() {
    return {
      name: this.name,
      failed: this.failed,
      completed: this.completed
    }
  }

  checkComplete() {
    console.log('Captain - checking completeness')
    if (this.failed.length >= this.identity.roundFails || this.players.length === 0) {
      console.log('Captain - too many orders Failed:', this.failed.length, this.failed)
      this.destructor()
      observer.send(this, constants.round.failed)
      this.complete = true
    } else if (Date.now() > this.targetTime) {
      console.log('Captain - Round ran out of time')
      this.destructor() // turn off events
      observer.send(this, constants.round.completed)
      this.complete = true
    }
    return this.complete
  }
}