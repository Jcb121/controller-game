import * as observer from 'node-observer'
import * as _constants from '../data/constants.json'
const constants: any = _constants;
const randomString = require('random-string')
import { layout, rootLayout } from '../interfaces/layouts';
import { player } from '../interfaces/player';
import { module, setupModule } from '../interfaces/modules';

interface constructor {
  layout: rootLayout,
  modules: Array<module>
}

export default class Player {
  id: string
  layout: rootLayout
  modules: Array<setupModule>
  ready: boolean
  score: number
  conn: any
  game: {
  }

  render(): player {
    return {     
      layout: this.layout,
      modules: this.modules,
    }
  }

  constructor({ layout, modules }: constructor) {
    this.id = randomString(16)
    this.layout = layout
    this.modules = this.setupPlayer(modules)
    this.ready = false
    this.score = 0
    // this needed to hw controller?
    // observer.subscribe(this, constants.player.setup, this.setupPlayerModules.bind(this))
  }

  addConnection(conn) {
    this.conn = conn
    conn.on('text', this.receive.bind(this))
    conn.on('error', this.socketError.bind(this))
    conn.on('close', this.leaveGame.bind(this))
    this.joinGame()
  }

  updateStage(payload) {
    this.send({
      type: constants.game.updateStage,
      payload
    })
  }

  changeStage(...args: string[]) {
    this.send({
      type: constants.game.changeStage,
      payload: [...arguments]
    })
  }

  setupPlayer(modules: Array<module>): Array<setupModule> {
    return modules.map(module => {

      let id = randomString(8)
      let state = module.capabilities.reduce((state, element) => {
        // todo, make sure it's off if a button.
        // todo, make sure one is on if radio
        state[element.key] = module.options[0].key
        return state
      }, {});
            
      return {
        ...module,
        id,
        state
      }

    })
  }

  // is this needed for the hardware controller?
  // setupPlayerModules(player, { modules }) {
  //   if (player.name !== this.name) {
  //     return
  //   }

  //   this.modules = modules
  //   this.ready = true
  //   console.log('Player - Assigning Modules IDS to player')

  //   let ids = []
  //   this.modules.forEach(module => {
  //     let id = randomString(8)
  //     module.id = id
  //     ids.push(id)
  //   })
  //   this.send({
  //     type: constants.serial.setIds,
  //     payload: ids
  //   })
  //   console.log(`Player - Player ${this.name} is setup`)
  // }

  receive(text) {
    // console.log('Got', text)
    try {
      var { type, payload } = JSON.parse(text)
    }
    catch (err) {
      console.log('Player - Failed to parse socket message as json.')
    }

    // Get the related module
    let playerModule = this.modules.find(mod => mod.id === payload.id)
    if (typeof playerModule === 'undefined') {
      console.log('Player - Failed to find matching Module from socket message, must be meta')
      playerModule = {
        name: '',
        type: '',
        id: '',
        state: {},
        element: '',
        location: {

        },
        options: [

        ],
        capabilities: [

        ]
      }
    }

    // update the player state
    if (type === constants.player.stateChange) {
      playerModule.state = payload.values
    }

    // this code could be moved to a listener...
    if (type === constants.player.buttonPress && // button press is still fine
      playerModule.type === 'START' && // module types are now consts
      payload.values === 'S') { // values == char (1 byte)
      observer.send(this, constants.game.start)
    } else {
      observer.send(this, type, payload)
    }
  }

  send(message) {
    if (typeof message !== 'string') {
      message = JSON.stringify(message)
    }

    try {
      this.conn.sendText(message)
    } catch (err) {
      console.log(err)
    }
  }

  leaveGame() {
    console.log('Player left!')
    observer.send(this, constants.player.left)
  }

  socketError(err) {
    console.log('SOCKET ERROR', err)
  }

  joinGame() {
    observer.send(this.game, constants.game.broadcast, {
      type: constants.player.message,
      payloaod: `player ${this.id} joined the game`
    })

    this.send({
      type: constants.player.rename,
      payload: {
        name: this.id,
      }
    })
    this.changeStage('Space', 'Waiting')
  }
}