import Captain from './captain'
import * as _constants from '../data/constants.json'
const constants: any = _constants;const captains = require ('captains')
import * as fs from 'fs'
import Player from './player';

var observer = require('node-observer')

export default class Game {
  players: Array<Player>
  captains: Array<Captain>
  highScores: Array<number>

  constructor() {

    console.log('Game Created')

    observer.subscribe(this, constants.player.joined, this.addPlayer.bind(this))
    observer.subscribe(this, constants.player.left, this.removePlayer.bind(this))
    observer.subscribe(this, constants.game.start, this.startNew.bind(this))
    observer.subscribe(this, constants.game.broadcast, this.broadcast.bind(this))
    observer.subscribe(this, constants.round.completed, this.roundCompleted.bind(this))
    observer.subscribe(this, constants.round.failed, this.roundFailed.bind(this))

    this.players = []
    this.captains = []
    this.highScores = []

    this.getHighScores()
  }

  getHighScores(){

    const fileName = 'dist/highScores.json'

    fs.exists(fileName, exists => {      
      if(exists){
        fs.readFile(fileName, 'utf8', (error, data) => {
          if(error){
            console.log('Error reading file', error)
          } else {
            this.highScores = JSON.parse(data)
          }
        })
      } else {
        console.log('High scores does not exsist, creating file')
        fs.writeFile(fileName, JSON.stringify(this.highScores), error => {
          console.log(error)
        })
      }
    })
  }

  roundCompleted(captain) {
    // does this captain belong to this game?
    if (this.captains.indexOf(captain) === -1) return

    console.log('Game - Round Complete')
    let teamScore = this.players.reduce((score, player) => {
      score += player.score
      return score
    }, 0)
    
    this.broadcast(this, {
      type: constants.game.changeStage,
      payload: ['Space', 'RoundOver']
    })
    
    this.players.forEach(player => {
      player.updateStage({
        captains: this.captains.map(captain => captain.render()),
        playerScore: player.score,
        teamScore
      })
    })
  }

  roundFailed(captain) {
    // does this captain belong to this game?
    if (this.captains.indexOf(captain) === -1) return

    let teamScore = this.players.reduce((score, player) => {
      score += player.score
      return score
    }, 0)

    console.log('Game - Round Failed, team score:', teamScore)

    this.broadcast(this, {
      type: constants.game.changeStage,
      payload: ['Space', 'GameOver']
    })

    this.players.forEach(player => {
      player.updateStage({
        captains: this.captains.map(captain => captain.render()),
        playerScore: player.score,
        teamScore
      })
      player.score = 0
    })
    this.captains = []
  }

  startNew(player: Player) {
    // does this player belong to this game
    if(this.players.indexOf(player) === -1) return
    
    console.log(`Game - ${player.id} pressed 'Start' button`)
    if (this.captains.length === 0 || this.captains[0].complete) {
      console.log(`Game - Round started! by ${player.id}`)
      this.broadcast(this, {
        type: constants.player.message,
        payload: `Round started! by ${player.id}`
      })
      this.newCaptain()
    } else {
      console.log('Game - round already in progress')
      let currentlyPlayinig = this.captains[0].players.find(_player => _player.id === player.id)
      if (typeof currentlyPlayinig !== 'undefined') {
        console.log(`Game - Player ${player.id} is already in round`)
        // check to continue the round
      } else {
        console.log(`Game - Player ${player.id} joined the Round`)
        this.broadcast(this, {
          type: constants.player.message,
          payload: `Game - Player ${player.id} joined the Round`
        })
        this.captains[0].addPlayer(player)
      }
    }
  }

  newCaptain() {
    console.log('Game - Creating new Captain', captains[0].name)
    this.captains.unshift(new Captain(this, captains[0], this.players))
    
    this.broadcast(this, {
      type: constants.game.changeStage,
      payload: ['Space', 'Missions']
    })

    this.broadcast(this, {
      type: constants.game.updateStage,
      payload: {
        roundNumber: this.captains.length,
        orderFails: 0 // reset the order Fails
      }
    })
  }

  addPlayer(room, player) {    
    if(room.game === this) {
      player.game = this
      console.log(`Player ${player.id} joined game`)
      this.players.push(player)
    }
  }

  removePlayer(player) {
    // does this player belong to this game
    if (this.players.indexOf(player) === -1) return

    console.log('Game - Removing player from Game', player.id)

    this.players = this.players.filter(_player => _player !== player)
    
    if (this.captains.length > 0) {
      this.captains[0].removePlayer(player)
    }
    
    if (this.players.length === 0){
      this.captains = []
    }
    this.broadcast(this, `player ${player.name} left the game`)
  }

  // this is a shit function!
  broadcast(game, message) {
    if(this !== game) return
    this.players.forEach(player => {
      player.send(message)
    })
  }
}