import * as fs from 'fs'
import { mission } from '../interfaces/mission'

export async function updateMission(req, res){
  let missions: Array<mission> = await readFile() 
  let index = missions.findIndex(m => m.name == req.body.name)
  if (index > -1) {
    missions[index] = req.body
  } else {
    missions.push(req.body)
  }
  await writeFile(missions)
  res.send('{"payload": "Mission Saved"}')
}

export async function deleteMission(req, res){
  let missions = await readFile()
  missions = missions.filter(m => m.name != req.params.missionName)
  res.send('{"payload": "Mission Deleted"}')
  writeFile(missions)
}

function readFile() {
  return new Promise<Array<mission>>((resolve, reject) => {
    fs.readFile('./data/missions.json', 'utf8', (err, data: string) => {
      if (err) {
        reject(err)
        return
      }
      resolve(JSON.parse(data))
      return
    })
  })
}

function writeFile(data) {
  return new Promise((resolve, reject) => {
    fs.writeFile('./data/missions.json', JSON.stringify(data), (err) => {
      if (err) {
        reject(err)
        return
      }
      resolve()
      return
    })
  })
}