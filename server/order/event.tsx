import BaseOrder from './base'
import * as observer from 'node-observer'
import * as _constants from '../../data/constants.json'
const constants: any = _constants;

export default class EventOrder extends BaseOrder {
  task: Array<string>

  constructor(captain, playerA, playerB, id, order, time) {
    super(captain, playerA, playerB, id, order, time)
    this.task = this.createTask(order, this.module)
    this.sentence = this.createSentence(this.task, this.module, order)
    observer.subscribe(this, constants.player.buttonPress, this.buttonPress.bind(this))
  }

  createTask(order, module) {
    let keys = module.capabilities.map(cap => cap.key)
    let task = this.getRandomItems(keys, order.maxLength, order.exclude)
    return task
  }

  buttonPress(player, { id, values }) {
    if (this.playerB !== player) {
      return
    }    
    if (this.module.id === id) {
      if (String(this.task[0]) === String(values)) {
        this.task.shift()
        observer.send(this, constants.player.correct, this.playerB)
      }
    } else {
      console.log('event/buttonpress', 'wrong module')
      observer.send(this, constants.player.wrongModule, this.playerB)
    }
    if(this.task.length === 0){
      this.checkComplete(true);
    }
  }

  createSentence(task, module, order) {
    // replace element labels...
    let labels = task.map(key => {
      return module.capabilities.find(element => element.key == key).label
    })

    return this.getRandomItem(order.sentences)
      .replace('%noun', module.name)
      .replace('%option', labels.join(', '))
  }

  getRandomItems(_array, upperBound = 3, exclude = []) {

    if (!Array.isArray(exclude)) {
      throw new Error('Exclude must be an array!')
    }

    upperBound = Math.ceil(Math.random() * upperBound)
    let items = []
    while (items.length < upperBound) {
      let item = this.getRandomItem(_array)
      if (exclude.indexOf(item) === -1) {
        items.push(item)
      }
    }
    return items
  }
}