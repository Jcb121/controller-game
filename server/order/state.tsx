import BaseOrder from './base'
import * as observer from 'node-observer'
import { module } from '../../interfaces/modules';
import * as _constants from '../../data/constants.json'
const constants: any = _constants;

export default class StateOrder extends BaseOrder {
  task: Object
  sentence: string

  constructor(captain, playerA, playerB, id, order, time) {
    super(captain, playerA, playerB, id, order, time)
    this.task = this.createTask(order, this.module)
    this.sentence = this.createSentence(this.task, this.module, order)
    observer.subscribe(this, constants.player.stateChange, this.stateChange.bind(this))
  }

  createTask(order, module) {
    let key = order.condition
    let state
    let options

    if (Array.isArray(module.options)) {
      options = module.options.map(module => module.key)
    } else {
      let upper = parseInt(module.options.split('-')[1])
      options = Array(upper + 1).fill('a').map((a, index) => index)
    }

    if (key === '.*' || key === '.') {
      // all, check if all states are already the same
      // any, don't set change to x if all the same
      // This get the state from the modules object
      let same = Object.keys(module.options).reduce((keyA, keyB) => {
        return (module.options[keyA] === module.options[keyB]) ? module.options[keyA] : ''
      })
      state = this.getRandomItem(options, same)
    } else {
      let keys = module.capabilities.map(cap => cap.key)
      key = this.getRandomItem(keys)
      state = this.getRandomItem(options, module.state[key])
    }
    return {
      [key]: state
    }
  }

  stateChange(player, { id, values }) {
    if (this.playerB !== player) {
      return
    }
    if (this.module.id === id) {
      if (this.compareState(this.task, values)) {
        this.checkComplete(true);
      }
    } else {
      observer.send(this, constants.player.wrongModule, this.playerB)
    }
  }

  compareState(a: Object, b: Object): boolean {
    // Import item keys
    // '.*' all must equal
    // '.' one must equal

    if (a.hasOwnProperty('.*')) {
      let temp = Object.keys(b).find(key => {
        return b[key] !== a['.*']
      })
      if (typeof temp === 'undefined') {
        return true
      }
      return false
    } else if (a.hasOwnProperty('.')) {
      throw new Error('. selelctor in item key not implemented yet!!')
    } else {
      let diff = true
      for (let key in a) {
        if (String(a[key]) !== String(b[key])) {
          diff = false
        }
      }
      return diff
    }
  }

  createSentence(task: object, module: module, order): string {
    let labels = []

    // replace device/state labels...
    task = Object.keys(task).reduce((_task, key) => {
      var state = task[key]
      if (key !== '.' && key !== '.*') {
        let label = module.capabilities.find(element => element.key == key).label
        _task[label] = task[key]        
      } else {
        _task[key] = order.options[state]
      }
      return _task
    }, {})

    if (order.option) {
      labels = Object.keys(task).map(key => {
        return order.option
          .replace('%element', key)
          .replace('%value', task[key])
      })
    } else {
      console.log('State - ORDER', order)
      labels = Object.keys(task) // issue here is the key is taken..
    }

    return this.getRandomItem(order.sentences)
      .replace('%noun', module.name)
      .replace('%option', labels.join(', '))
  }
}