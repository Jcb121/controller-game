const observer = require('node-observer')
import * as _constants from '../../data/constants.json'
const constants: any = _constants;

import { renderedMission } from '../../interfaces/mission'
import Player from '../player';
import Captain from '../captain';
import { setupModule } from '../../interfaces/modules';


export default class BaseOrder {

  playerA: Player
  playerB: Player
  captain: Captain
  module: setupModule
  targetTime: number
  timeOut: any
  task: Array<any> | Object
  sentence: string

  constructor(captain, playerA, playerB, moduleId, order, time){
    if (typeof captain === 'undefined') {
      throw new Error('captain in order cannot not be undefined!')

    }
    if (typeof playerA === 'undefined' || typeof playerB === 'undefined') {
      throw new Error('Players in order cannot not be undefined!')
    }
    this.playerA = playerA // Reader
    this.playerB = playerB // Target
    this.captain = captain

    // get the module being worked on
    this.module = playerB.modules.find(module => module.id === moduleId)

    this.targetTime = Date.now() + time * 1000
    this.timeOut = setTimeout(function () {
      console.log('Order - Timeout fired')
      this.checkComplete(false)
    }.bind(this), time * 1000)
  }

  checkComplete(complete: boolean) {
    this.destructor() // turn off events
    if(complete) {
      observer.send(this, constants.order.completed)
    } else {
      observer.send(this, constants.order.failed) 
      // send that they have failed
    }
  }
  
  destructor() {
    clearTimeout(this.timeOut)
    observer.unsubscribe(this, constants.player.buttonPress)
    observer.unsubscribe(this, constants.player.stateChange)
  }
  
  render(): renderedMission {
    return {
      sentence: this.sentence,
      targetTime: this.targetTime
    }
  }

  getRandomItem(_array, exclude?) {
    let validItems = _array.filter(item => item !== exclude)
    return validItems[Math.floor(Math.random() * validItems.length)]
  }
}