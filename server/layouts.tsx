import * as fs from 'fs'
import { layout } from '../interfaces/layouts';

export async function getLayout(req, res){
  let layouts = await readFile()
  let layout = layouts.find(layout => layout.layoutName == req.params.layoutName)
  res.send(JSON.stringify(layout))
}

export async function getAnyLayout(req, res){
  let layouts = await readFile()
  let layout = layouts[Math.floor(Math.random() * layouts.length)]
  res.send(JSON.stringify(layout))
}

export async function deleteLayout(req, res){
  let layouts = await readFile()
  layouts = layouts.filter(layout => layout.layoutName != req.params.layoutName)
  await writeFile(layouts)
  res.send('{"payload": "Design Deleted"}')
}

export async function layoutNames(req, res){
  let layouts = await readFile()
  let layoutName = layouts.map(layout => layout.layoutName)
  res.send(JSON.stringify(layoutName))
}

export async function updateLayout(req, res){
  if (!req.body.layoutName) {
    res.status(400).send('{"payload": "Missing `layoutName`"}')
    return
  }
  let layouts = await readFile()
  let layout = layouts.find(layout => layout.layoutName == req.body.layoutName)
  if (layout) {
    layouts[layouts.indexOf(layout)] = req.body
  } else {
    layouts.push(req.body)
  }
  await writeFile(layouts)
  res.send('{"payload": "Design Saved"}')
}

function readFile() {
  return new Promise<Array<layout>>((resolve, reject) => {
    fs.readFile('./data/layouts.json', 'utf8', (err, data) => {
      if (err) {
        reject(err)
        return
      }

      let layouts:Array<layout> = JSON.parse(data)
      resolve(layouts)
      return
    })
  })
}

function writeFile(data) {
  return new Promise((resolve, reject) => {
    fs.writeFile('./data/layouts.json', JSON.stringify(data), (err) => {
      if (err) {
        reject(err)
        return
      }
      resolve()
      return
    })
  })
}