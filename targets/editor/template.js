export default ({ body, initialState }) => {
  return `
    <!DOCTYPE html>
    <html>
      <head>
        <title>Editor</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script>window.__APP_INITIAL_STATE__ = ${initialState}</script>
      </head>
      
      <div class="body"> 
        <div id="root">${body}</div>
      </div>
      <script type="text/javascript" src="/editor.bundle.js"></script>
    </html>
  `
}