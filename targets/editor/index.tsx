declare global {
  interface Window { __APP_INITIAL_STATE__: any; }
}

const s = require('./styles.scss')

import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import Editor from '../../components/editor'
import { Provider } from 'react-redux'
import editorApp from '../../components/editor/reducers'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'

const initialState = window.__APP_INITIAL_STATE__
delete window.__APP_INITIAL_STATE__
const store = createStore(editorApp, initialState, applyMiddleware(
  thunkMiddleware
))

const root = document.getElementById('root')
ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Editor />
    </BrowserRouter>
  </Provider>
  , root)
