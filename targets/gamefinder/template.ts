export default ({ body, title, initialState }) => {
  return `
    <!DOCTYPE html>
    <html>
      <head>
        <title>${title}</title>
        <script>window.__APP_INITIAL_STATE__ = ${initialState}</script>
      </head>
      
      <body>
        <div id="root">${body}</div>
      </body>
      
      <script src="gamefinder.bundle.js"></script>
    </html>
  `
}