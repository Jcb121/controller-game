import * as React from 'react'
import { render } from 'react-dom'
import GameFinder from '../../components/finder'
declare global {
  interface Window { __APP_INITIAL_STATE__: any; }
}

render(<GameFinder {...window.__APP_INITIAL_STATE__}/>, document.getElementById('root'))
