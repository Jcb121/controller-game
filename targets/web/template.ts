export default ({ body, title, initialState }) => {
  return `
    <!DOCTYPE html>
    <html>
      <head>
        <title>${title}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script>window.__APP_INITIAL_STATE__ = ${initialState}</script>
      </head>
      
      <div class="body">
        <div class="panel">
          <canvas id="mission"></canvas>
        </div>

        <div class="controls">
          <div id="root">${body}</div>
        </div>
      </div>
      <script type="text/javascript" src="/web.bundle.js"></script>
    </html>
  `
}