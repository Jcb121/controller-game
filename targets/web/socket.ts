import BaseSocket from '../../client/classes/basesocket'
// no ws import needed

export default class Socket extends BaseSocket {
  constructor(room, userName){
    super()
    this.socket = new WebSocket(`ws://${window.location.hostname}:8001/${room}/${userName}`)
    this.socket.onopen = this.open.bind(this)
    this.socket.onerror = this.error.bind(this)
    this.socket.onclose = this.close.bind(this)
  }
}
