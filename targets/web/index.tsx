console.log('Web start!')

declare global {
  interface Window { __APP_INITIAL_STATE__: any; }
}

import * as observer from 'node-observer'
import * as _constants from '../../data/constants.json'
const constants: any = _constants;

import Socket from './socket'
import Game from '../../client' // needed?
import Webcontroller from '../../components/webcontroller'
import * as React from 'react'
import * as ReactDOM from 'react-dom'

import GameMusic from '../../components/music/index'
new GameMusic()

//web is full web game. controller + missions.
const s = require('./styles.scss')
import panel from '../../components/missionpanel/index'
panel()
const s2 = require('../../components/webcontroller/styles.scss')

const playerId = window.__APP_INITIAL_STATE__.player.id

let path = window.location.pathname.replace('/room/', '')
new Socket(path, playerId)

observer.subscribe('Web Index', constants.connection.open, () => {
  new Game()
})

const root = document.getElementById('root')
ReactDOM.hydrate(<Webcontroller {...window.__APP_INITIAL_STATE__}/>, root);