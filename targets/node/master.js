import to from 'await-to-js'
import constants from '../../data/constants'
import devices from '../../data/devices'

export default class Master {
  constructor(serialDevice){
    this.device = serialDevice
    this.modules = {}
  }

  async setupMaster(){
    let [error, modulesLabels] = await to(this.getModuleTypes())
    if(error){
      console.log('Failed to get master\'s label types')
    }

    modulesLabels.forEach(moduleType => {
      this.modules[ransdomString(8)] = moduleType
    })

    let [error2, modsObject] = await to(this.setModuleIds())
    if(error2){
      console.log('Failed to set masters\' ids')
    }
    return(modsObject)
  }
  async getModuleTypes(){
    console.log('GETTING LABELS, SENDING:', constants.serial.listModules)
    let [err, response] = await to(this.device.sendAndReceive(constants.serial.listModules))
    if(err){
      console.log('Failed to get moduleIDs')
      return false
    }
    return response.split(',').map(module => {
      return module.trim()
    })
  }
  async setModuleIds(){
    let argument = Object.keys(this.modules).join(',')
    let [error, response] = await to(this.device.sendAndReceive(constants.serial.setIds, argument))
    if(error || response === 'FALSE'){
      console.log('Failed assign ids to get Master')
      return false
    } else {
      return true;
    }
  }
}