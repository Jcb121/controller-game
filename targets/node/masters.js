import to from 'await-to-js'
import SerialDevice from './serialdevice'
import SerialPort from 'serialport'
import Master from './master'

export default class MasterManager {
  constructor(){
    this.masters = []
  }
  async getMasters(){
    let [err, masters] = await to(this.findGameMasters())
    if(err){
      throw new Error('failed to get game masters')
    }
    masters = masters.filter(master => { return master })
    this.masters = masters.map(device => new Master(device))
    
    // setup the modules
    let proms = this.masters.map(master => master.setupMaster())
    let done = await Promise.all(proms)
    let notDone = done.find(state => state === false);
    if(notDone === 'undefined'){
      throw new Error('A Master failed to setup!')
    }
    return this.masters
  }
  async findGameMasters() {
    try {
      let devices = await SerialPort.list()
      let promises = devices.map(this.isGameMaster)
      let modules = await Promise.all(promises)
      return modules
    } catch (error) {
      throw new Error(error)
    }
  }
  async isGameMaster(device) {
    let name = device.comName
    let serialDevice = new SerialDevice(name)
    let [error, response] = await to(serialDevice.isGameMaster())
    if (error || response === false) {
      return false
    } else {
      return serialDevice
    }
  }
}