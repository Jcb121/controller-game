import constants from '../../data/constants'
import observer from 'node-observer'

console.log('Node Start')
import to from 'await-to-js'
import Game from '../../client'
new Game()


import Socket from './socket'
new Socket()

import MasterManager from './masters'
class Node {

  constructor(){
    this.setup().then(masters => {
      console.log('These are my masters', masters);
      observer.subscribe(this, constants.serial.eventBuffer, this.handlEventBuffer)
      observer.subscribe(this, constants.serial.stateBuffer, this.handleStateBuffer)
      observer.subscribe(this, constants.serial.receive, (serialDeive, line) => {
        let [id, command, data] = line.split(constants.serial.delimeter)
        data.trim()
        observer.send(id, command, data);
      })
    })
  }
  
  async setup(){
    this.manager = new MasterManager()
    let [error, masters] = await to(this.manager.getMasters())
    if(error){
      throw new error('Failed to get masters')
    }
    return masters;
  }

  handleStateBuffer(id, state){
    observer.send(this, constants.connection.send, {
      type: constants.player.stateChange,
      payload: {
        id,
        values: state
      }
    })
  }

  handlEventBuffer(id, buffer){
    buffer.split('').forEach(element => {
      observer.send(this, constants.connection.send, {
        type: constants.player.buttonPress,
        payload: {
          id,
          values: element
        }
      })  
    }) 
  }
}
new Node()

import devices from '../../data/devices'
