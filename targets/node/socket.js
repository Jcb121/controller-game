import BaseSocket from '../../client/classes/basesocket'
import WebSocket from 'ws'

export default class Socket extends BaseSocket {
  constructor(path){
    super()
    this.socket = new WebSocket (`ws://controllergame.local:8001/${path}`)
    this.socket.onopen = this.open.bind(this)
    this.socket.onerror = this.error.bind(this)
    this.socket.onclose = this.close.bind(this)
  }
}
