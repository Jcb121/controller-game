import SerialPort from 'serialport'
const Readline = SerialPort.parsers.Readline
import { baudRate } from '../../data/config.json' // get baudRate

import constants from '../../data/constants.json'
import observer from 'node-observer'
import to from 'await-to-js'


export default class SerialDevice {
  constructor(name){
    this.name = name
    this.backlog = []
    this.waitingFor = []
    this.ready = false
    this.port = new SerialPort(name, { baudRate })
    this.parser = new Readline()
    this.port.pipe(this.parser)
    this.parser.on('data', this.receive.bind(this))
    this.port.on('close', this.error.bind(this))   
    this.port.on('error', this.error.bind(this))
    this.port.on('open', function(){console.log('PORT OPEN')}.bind(this))
  }
  close(err){
    console.log('SERIAL Close: ', err.message)
  }
  error(err){
    console.log('Failed to open serial deive:', this.name)
    // console.log('SERIAL Error: ', err.message)
  }
  async sendAndReceive(message, argument = ''){
    let messageID = new Array(8).fill(false).map(() => {
      return Math.floor(Math.random() * 10)
    }).join('')

    this.send(message, messageID, argument)
    let [error, response] = await to(this.await(messageID))
    this.busy = false
    this.processBacklog()
    if(error){
      throw new Error('No response, implemented')
    }
    return response
  }
  send(command, id = '00000000', argument = '') {
    if(this.ready === false || this.busy === true){
      this.backlog.push({ id, command, argument})
    } else {
      this.busy = true
      let message = id + '|' + command + '|' + argument + '\n'
      console.log('SERIAL send', message)
      this.port.write(message, function (err) {
        if (err){
          throw new Error(err)
        }
      })
    }
  }
  processBacklog(){
    let task = this.backlog.shift()
    if (task) {
      this.send(task.command, task.id, task.argument)
    }
  }
  receive(line){
    line = line.trim()
    if(!line) return
    let messageID = line.substring(0,8)
    let message = line.substring(9)
    console.log('SERIAL message IN', line)
    observer.send(this, constants.serial.receive, line)

    if(message === constants.serial.ready && this.ready === false){
      this.ready = true
      this.processBacklog()
    }

    this.waitingFor.reduce((queue, waiting) => {
      if (messageID === waiting.id){
        waiting.resolve(message)
      } else {
        queue.push(waiting)
      }
      return queue
    }, [])
  }
  await(id){
    return new Promise((resolve, reject) => {
      this.waitingFor.push({
        id,
        resolve
      })
      setTimeout(() => {
        this.waitingFor = this.waitingFor.reduce((queue, waiting) => {
          if (waiting.id !== id) queue.push(waiting)
          return queue
        }, [])
        reject(true)  
      }, 5000)
    })
  }
  async isGameMaster(){
    let [error, response] = await to(this.sendAndReceive(constants.serial.isGameMaster))
    if(error || response === 'FALSE'){
      console.log('Failed Response to get Master')
      return false
    } else {
      return true
    }
  }
}